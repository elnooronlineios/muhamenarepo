//
//  MainViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import DropDown
import SDWebImage
import Hero
var didPick = false
class MainViewController: UIViewController, UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet var lawyersTable: UITableView!
    let picker = UIPickerView()
    let dropdown = DropDown()
    var cities = CityMain(){
        didSet{
            self.allLawyers.removeAll()
            self.pageNumber = 0
            self.cityId = self.cities.data?.first?.id ?? 0
            self.textfield.text = self.cities.data?.first?.name ?? ""
            self.getLawyers(page: self.pageNumber)
            self.paginationFlag = false

        }
    }
    var lawyers = Lawyers()
    var allLawyers = [Lawyer](){
        didSet{
            self.tableView.reloadData()
        }
    }
    var paginationFlag = false
    @IBOutlet weak var textfield: UITextField!
    var cityId = 0
    var pageNumber = 0
    var cityName = ""
    @IBAction func notification_btn(_ sender: UIButton) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        
        self.navigationController?.hero.navigationAnimationType = .auto
        self.navigationController?.pushViewController(navVC, animated: true)
        
    }
    @IBAction func searchPressed(_ sender: UIButton) {
        dropdown.direction = .bottom
        dropdown.anchorView = sender
        dropdown.dataSource = cities.getNames()
        dropdown.width = 150
        dropdown.contentMode = .center
        dropdown.bottomOffset = CGPoint(x:0,y:sender.frame.height+1)
        dropdown.textFont = UIFont(name: "NeoSansArabic", size: 14) ?? UIFont.systemFont(ofSize: 15)
        dropdown.show()
        dropdown.selectionAction = {[unowned self](index: Int, item: String) in
            self.allLawyers.removeAll()
            self.pageNumber = 0
            self.cityId = self.cities.data![index].id ?? 0
            self.textfield.text = self.cities.data![index].name ?? ""
            self.getLawyers(page: self.pageNumber)
            self.paginationFlag = false
        }
        
    }
    
    @IBAction func menuPressed(_ sender: Any) {
        let vc = AppStoryboard.Menus.viewController(viewControllerClass: MenuVC.self)
        vc.modalPresentationStyle = .overFullScreen
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var notification_icon: UIButton!
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        super.viewDidLoad()
        pageNumber = 0
        if !User.shared.isLoggedIn() {
            notification_icon.isHidden  = true
        }else if UserDefaults.standard.bool(forKey: "presentedCountryPicker") {
            getLawyers(page: pageNumber)
        }
        self.hero.isEnabled = true
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        configureTableView()
        getCities()
        tableView.separatorStyle = .none
        NotificationCenter.default.addObserver(self, selector: #selector(getLawyersFromSelection), name: NSNotification.Name(rawValue: "ReloadLawyers"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("pressNotificationForUser"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openRegulationsShow), name: NSNotification.Name(rawValue: "OpenRegulations"), object: nil)
    }
    
        
        @objc func methodOfReceivedNotification(notification: Notification) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "TalabatiViewController") as! TalabatiViewController
            self.navigationController?.pushViewController(navVC, animated: true)
                        if let body = notification.userInfo?["body"] as? String,  let title = notification.userInfo?["title"] as? String {
                            makeDoneAlert(title: title, SubTitle: body, Image: #imageLiteral(resourceName: "icon13"))
                         }
            
    }

    @objc func getLawyersFromSelection(_ notification: NSNotification) {
        if let dict = notification.userInfo {
            if let id = dict["id"] as? Int, let name = dict["name"] as? String{
                self.allLawyers.removeAll()
                self.pageNumber = 0
                self.textfield.text = name
                self.cityId = id
                self.getLawyers(page: pageNumber)
                self.paginationFlag = false
                
            }
        }
    }
//    override func viewDidAppear(_ animated: Bool) {
//        pageNumber = 0
//        if didPick {
//            getLawyers(page: pageNumber)
//            didPick = false
//        }
//    }
    @objc func getLawyers(page:Int){
        HUD.show(.progress)
        print(cityId)
        var id = cityId
        if id == 0 {
            id = UserDefaults.standard.integer(forKey: "countryId")
        }
        if id == 0 {
            HUD.hide()
            return
        }
        print(APIs.Instance.getLaywers(id:id, page: page))
        print(APIs.Instance.getHeader())
        callApi(withURL: APIs.Instance.getLaywers(id:id, page: page), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            HUD.hide()
            switch result {
            case .success(let data):
                print(data)
                do {
                    self.lawyers = try JSONDecoder().decode(Lawyers.self, from: data)
                    
                    if let lawyerData = self.lawyers.data  {
                    for lawyer in lawyerData {
                        self.allLawyers.append(lawyer)
                        print(self.allLawyers.count ?? 0 )
                    }
                    }
                    if let get_count = self.lawyers.data {
                        if get_count.count ==  0 {
                            self.paginationFlag = true
                            if self.pageNumber > 1 {
                                return
                            }else{
                            makeDoneAlert(title: "", SubTitle: "عفوا لا يوجد محامين في هذه المدينة حاليا", Image: #imageLiteral(resourceName: "icon13"))
                            }
                        }
                        self.pageNumber += 1
                    }
                    
                   
                }catch{
                   self.lawyers = Lawyers()
                }
                
                
            default:
                
                break
            }
            self.lawyersTable.reloadData()
        }
    }
    
    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(allLawyers.count ?? 0)
        return allLawyers.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ElRaeseyaCell", for: indexPath)
        let namelbl = cell.viewWithTag(1) as! UILabel
        let cityLbl = cell.viewWithTag(2) as! UILabel
        let imgView = cell.viewWithTag(3) as! UIImageView
        cell.selectionStyle = .none
        namelbl.hero.id = "\(indexPath.row)-1"
        cityLbl.hero.id = "\(indexPath.row)-2"
        imgView.hero.id = "\(indexPath.row)-3"
        cell.viewWithTag(4)?.hero.id = "\(indexPath.row)-4"
        print(namelbl.hero.id)
        let item = allLawyers[indexPath.row]
        namelbl.text = item.name ?? ""
        cityLbl.text = item.city?.name ?? ""
        imgView.sd_setImage(with: URL(string:item.avatar ?? ""), completed: nil)
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "LawyersViewController") as! LawyersViewController
        navVC.hero.isEnabled = true
        navVC.heroId = "\(indexPath.row)"
        navVC.laywer = allLawyers[indexPath.row] ?? Lawyer()
        self.navigationController?.hero.navigationAnimationType = .auto
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if pageNumber < self.lawyers.meta?.last_page ?? 1 {
            
        if indexPath.row + 1 == allLawyers.count ?? 0 {
            print("do something")
            if paginationFlag == true {
                return
            }
            pageNumber += 1
            getLawyers(page: pageNumber)
            }
        }
    }
    func getCities(){
        // HUD.show(.progress)
        print(APIs.Instance.getCities())
        callApi(withURL: APIs.Instance.getCities(), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            switch result {
            case .success(let data):
                print(data)
                do {
                    self.cities = try JSONDecoder().decode(CityMain.self, from: data)
                }catch{
                    print(error)
                }
                
            default:
                break
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.paginationFlag = false
//        pageNumber = 0
        setupNavBar(vc: self)
    }
    
    
    func add_nav_btn(){
        
//        let editImage    = UIImage(named: "plus")!
//        let searchImage  = UIImage(named: "search")!
//        
//        let editButton   = UIBarButtonItem(image: editImage,  style: .Plain, target: self, action: "didTapEditButton:")
//        let searchButton = UIBarButtonItem(image: searchImage,  style: .Plain, target: self, action: "didTapSearchButton:")
//        
//        navigationItem.rightBarButtonItems = [editButton, searchButton]
    }
    
}

extension MainViewController: OpenViewControllersAfterDismiss {
    func openLoginViewController() {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: laweyrOrUserVC.self)
        //vc.hideButton = true
        let navVc = UINavigationController(rootViewController: vc)
        //        self.navigationController?.pushViewController(navVc, animated: true)
        self.navigationController?.present(navVc, animated: true, completion: nil)
    }
    
    func openMyNotification() {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: NotificationVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func openRegulationsShow() {
        let vc = AppStoryboard.Menus.viewController(viewControllerClass: RegulationsShowVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openMyOrders() {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: TalabatiViewController.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openMyProfile() {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: ProfileVC.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func openContactUs() {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: ContactUs.self)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
