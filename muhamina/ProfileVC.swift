//
//  ProfileVC.swift
//  muhamina
//
//  Created by mouhammed ali on 11/4/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import TextFieldEffects

class ProfileVC: UIViewController {
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var userImage: UIImageView!
    @IBOutlet var addressHeight: NSLayoutConstraint!
    @IBOutlet var addressLbl: HoshiTextField!
    @IBOutlet var emailLbl: HoshiTextField!
    @IBOutlet var mobileNumberLbl: HoshiTextField!
    
    @IBAction func settingBtn(_ sender: UIBarButtonItem) {
        let alertViewController = UIAlertController(title: "", message: "الخيارات", preferredStyle: .actionSheet)
        let editProfile = UIAlertAction(title: "تعديل الملف الشخصي", style: .default, handler: { (alert) in
            let vc = AppStoryboard.Main.viewController(viewControllerClass: EditProfileVC.self)
            self.navigationController?.pushViewController(vc, animated: true)
        })
        let logout = UIAlertAction(title: "تسجيل الخروج", style: .default) { (alert) in
            User.shared.logout()
            User.shared.fetchUser()
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier1"), object: nil)
            let vc = AppStoryboard.Main.viewController(viewControllerClass: laweyrOrUserVC.self)
            //vc.hideButton = true
            let navVc = UINavigationController(rootViewController: vc)
            self.present(navVc, animated: true, completion: nil)
            
        }
        let cancel = UIAlertAction(title: "إلغاء", style: .cancel) { (alert) in
            
        }
        alertViewController.addAction(editProfile)
        alertViewController.addAction(logout)
        alertViewController.addAction(cancel)
        alertViewController.popoverPresentationController?.sourceView = self.view
        alertViewController.popoverPresentationController?.permittedArrowDirections = .up
        alertViewController.popoverPresentationController?.sourceRect = CGRect(x: 0, y: 0, width: 10, height: 10)
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if User.shared.data?.type == "user" {
            addressHeight.constant = 0
            self.view.layoutIfNeeded()
            }
        if User.shared.data?.type == "lawyer" {
            addressLbl.text = User.shared.data?.city?.name ?? ""
        }
        userImage.sd_setImage(with: URL(string:User.shared.data?.avatar ?? ""), completed: nil)
        nameLbl.text = User.shared.data?.name ?? ""
        emailLbl.text = User.shared.data?.email ?? ""
        mobileNumberLbl.text = User.shared.data?.mobile ?? ""
        mobileNumberLbl.isEnabled = false
        addressLbl.isEnabled = false
        emailLbl.isEnabled = false
        nameLbl.isEnabled = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }

}
