//
//  laweyrOrUserVC.swift
//  muhamina
//
//  Created by mouhammed ali on 7/29/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import Hero

class laweyrOrUserVC: UIViewController {
    var animateFlag = true
    @IBOutlet var logoImgView: UIImageView!
    
    @IBOutlet var mainImg: UIImageView!
    @IBOutlet var loginBtn: UIButton!
    var button = UIButton()
    
    @IBOutlet var btnTopConstr: NSLayoutConstraint!
    @IBOutlet var logoHeightConstr: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hero.isEnabled = true
        loginBtn.hero.id = "Ali"
     //
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }
    override func viewDidAppear(_ animated: Bool) {
        
        if animateFlag {
            animateLogo()
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func animateLogo() {
        animateFlag = false
        UIView.animate(withDuration: 0.6, animations: { () -> Void in
            self.logoHeightConstr.constant += -40
               // .frame.addHeight(h: -40)
            self.btnTopConstr.constant += -10
          //  frame.addY(y: -10)
    self.view.layoutIfNeeded()
    })}
    
    @IBAction func pushToLoginPressed(_ sender: UIButton) {
        button.hero.id = ""
        button = sender
        let vc = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        vc.lawyerFlag = sender.tag
        sender.hero.id = "Ali"
        vc.hero.isEnabled = true
        self.navigationController?.hero.isEnabled = true
        self.navigationController?.hero.navigationAnimationType = .fade
        self.navigationController?.pushViewController(vc, animated: true)
        
       // present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func iraqianRegulationsTapped(_ sender: Any) {
        let vc = AppStoryboard.Menus.viewController(viewControllerClass: RegulationsShowVC.self)
        vc.modalPresentationStyle = .overFullScreen
        self.navigationController?.pushViewController(vc, animated: true)
//        self.dismiss(animated: true) {
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "OpenRegulations"), object: nil)
//        }
    }
}
