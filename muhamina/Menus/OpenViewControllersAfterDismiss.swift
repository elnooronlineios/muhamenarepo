//
//  OpenViewControllersAfterDismiss.swift
//  muhamina
//
//  Created by Mohamed on 6/11/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import Foundation
protocol OpenViewControllersAfterDismiss {
    func openLoginViewController()
    func openMyNotification()
    func openRegulationsShow()
    func openMyOrders()
    func openMyProfile()
    func openContactUs()
}

//MARK:- optional methods
extension OpenViewControllersAfterDismiss {
    func openLoginViewController() {
        
    }
    
    func openMyNotification() {
        
    }
    
    func openRegulationsShow() {
        
    }
    
    func openMyOrders() {
        
    }
    
    func openMyProfile() {
        
    }
    
    func openContactUs() {
        
    }
}
