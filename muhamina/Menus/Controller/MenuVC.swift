//
//  MenuVC.swift
//  muhamina
//
//  Created by Elsaadany on 6/8/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    
//    MARK: Properties
    
    let menuTitles = ["الاشعارات","القوانين العراقية", "طلباتي", "صفحتي الشخصية", "تواصل معنا"]
    var delegate: OpenViewControllersAfterDismiss?
    
//    MARK: Outlets
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logOutLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.tableHeaderView = UIView.init(frame:CGRect(x: 0.0, y: 0.0, width: self.tableView.bounds.size.width, height: 0.01))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if User.shared.isLoggedIn() {
            logOutLabel.text = NSLocalizedString("تسجيل الخروج", comment: "")
        } else {
            logOutLabel.text = NSLocalizedString("تسجيل الدخول", comment: "")
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        topView.setRadius(for: [.topLeft, .topRight], with: 55)
    }
    
//    MARK: IBActions
    @IBAction func signOutTapped(_ sender: Any) {
        
        if User.shared.isLoggedIn() {
            User.shared.logout()
            logOutLabel.text = NSLocalizedString("تسجيل الدخول", comment: "")
            NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier1"), object: nil)
        } else {
            self.dismiss(animated: true, completion: nil)
            delegate?.openLoginViewController()
        }
        
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- TableView
extension MenuVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        cell.titleLabel.text = NSLocalizedString(menuTitles[indexPath.row], comment: "") 
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if !User.shared.isLoggedIn() {
            let vc = AppStoryboard.Main.viewController(viewControllerClass: laweyrOrUserVC.self)
            let navVc = UINavigationController(rootViewController: vc)
            self.present(navVc, animated: true, completion: nil)
            return
        }
        
        switch indexPath.row {
        case 0:
            delegate?.openMyNotification()
            self.dismiss(animated: true, completion: nil)
        case 1:
            delegate?.openRegulationsShow()
            self.dismiss(animated: true, completion: nil)
        case 2:
            delegate?.openMyOrders()
            self.dismiss(animated: true, completion: nil)
        case 3:
            delegate?.openMyProfile()
            self.dismiss(animated: true, completion: nil)
        case 4:
            delegate?.openContactUs()
            self.dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
}
