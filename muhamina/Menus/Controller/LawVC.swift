//
//  LawVC.swift
//  muhamina
//
//  Created by Elsaadany on 6/8/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class LawVC: UIViewController {
    
    //    MARK: Properties
    var lawId: Int?
    
    //    MARK: Outlets
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        topView.setRadius(for: [.topLeft, .topRight], with: 55)
    }
    
    //    MARK: Functions
    
    //    MARK: IBActions
    @IBAction func showArticleTapped(_ sender: Any) {
        let vc = AppStoryboard.Menus.viewController(viewControllerClass: LawDetailsVC.self)
        vc.lawId = lawId
        vc.query = searchTF.text
        vc.navigationItem.title = self.navigationItem.title ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func showAllArticlesTapped(_ sender: Any) {
        let vc = AppStoryboard.Menus.viewController(viewControllerClass: LawDetailsVC.self)
        vc.lawId = lawId
        vc.navigationItem.title = self.navigationItem.title ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
