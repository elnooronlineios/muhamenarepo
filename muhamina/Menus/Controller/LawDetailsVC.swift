//
//  LawDetailsVC.swift
//  muhamina
//
//  Created by Elsaadany on 6/8/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class LawDetailsVC: UIViewController {
    
    //    MARK: Properties
    var lawId: Int?
    var query: String?
    var regulation: RegulationsData? {
        didSet {
           tableView.reloadData()
        }
    }
    
    //    MARK: Outlets
        
        @IBOutlet weak var topView: UIView!
        @IBOutlet weak var tableView: UITableView!
        @IBOutlet weak var searchTF: UITextField!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            tableView.delegate = self
            tableView.dataSource = self
            
            
            
            if let id = lawId {
                if let query = self.query {
                    getRegulation(url: APIs.Instance.searchRegulationData(id, and: query))
                } else {
                    getRegulation(url: APIs.Instance.getRegulationData(id))
                }
            }
        }
        
        override func viewWillLayoutSubviews() {
            super.viewWillLayoutSubviews()
            topView.setRadius(for: [.topLeft, .topRight], with: 55)
        }
        
        //    MARK: Functions
    }

    //MARK:- TableView
    extension LawDetailsVC: UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return regulation?.data?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LawDetailsCell", for: indexPath) as! LawDetailsCell
            cell.configure(with: regulation?.data?[indexPath.row])
            return cell
        }
        
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return UITableViewAutomaticDimension
        }
    }

//    MARK:- Networking
extension LawDetailsVC {
    func getRegulation(url: String) {
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(url.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? "", method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseString { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        makeErrorAlert(subTitle:err.parseError())
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        let parsedRegulation = try JSONDecoder().decode(RegulationsData.self, from: response.data!)
                        self.regulation = parsedRegulation
                        
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
