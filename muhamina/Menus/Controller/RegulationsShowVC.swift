//
//  RegulationsShowVC.swift
//  muhamina
//
//  Created by Elsaadany on 6/8/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire

class RegulationsShowVC: UIViewController {
    
//    MARK: Properties
    var regulations: Regulations? {
        didSet {
            tableView.reloadData()
        }
    }
    
    //    MARK: Outlets
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationItem.title = NSLocalizedString("القوانين العراقية", comment: "")
        searchTF.delegate = self
        getRegulations(url: APIs.Instance.getRegulations())
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        topView.setRadius(for: [.topLeft, .topRight], with: 55)
    }
    
    //    MARK: Functions
}

//MARK:- TableView
extension RegulationsShowVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return regulations?.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubMenuCell", for: indexPath) as! SubMenuCell
        cell.configure(with: regulations?.data?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppStoryboard.Menus.viewController(viewControllerClass: LawVC.self)
        vc.lawId = regulations?.data?[indexPath.row].id ?? -1
        vc.navigationItem.title =  regulations?.data?[indexPath.row].name ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK:- UITextFieldDelegate
extension RegulationsShowVC: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print(textField.text)
        if textField.text == "" {
            getRegulations(url: APIs.Instance.getRegulations().addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? "")
            return true
        } else if let name = textField.text {
            getRegulations(url: APIs.Instance.searchRegulations(name).addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) ?? "")
            return true
        }
        return true
    }
}

//MARK:- Networking
extension RegulationsShowVC {
    func getRegulations(url: String) {
        let header = APIs.Instance.getHeader()
        HUD.show(.progress, onView: self.view)
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: header).responseString { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        makeErrorAlert(subTitle:err.parseError())
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                        let parsedRegulations = try JSONDecoder().decode(Regulations.self, from: response.data!)
                        self.regulations = parsedRegulations
                        
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
    }
}
