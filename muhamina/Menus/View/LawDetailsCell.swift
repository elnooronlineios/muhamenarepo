//
//  LawDetailsCell.swift
//  muhamina
//
//  Created by Elsaadany on 6/8/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import UIKit

class LawDetailsCell: UITableViewCell {

    //    MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var parentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        parentView.addShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with regulation: RegulationsDetails?) {
        self.titleLabel.attributedText = (regulation?.name ?? "").html2AttributedString
             self.titleLabel.textAlignment = .right
             self.contentLabel.attributedText = (regulation?.description ?? "").html2AttributedString
             self.contentLabel.textAlignment = .right
    }

}
extension Data {
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print("error:", error)
            return  nil
        }
    }
    var html2String: String { html2AttributedString?.string ?? "" }
}

extension StringProtocol {
    var html2AttributedString: NSAttributedString? {
        Data(utf8).html2AttributedString
    }
    var html2String: String {
        html2AttributedString?.string ?? ""
    }
}
