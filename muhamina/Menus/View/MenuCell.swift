//
//  MenuCell.swift
//  muhamina
//
//  Created by Elsaadany on 6/8/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
//    MARK: Outlets
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
