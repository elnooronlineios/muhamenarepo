//
//  SubMenuCell.swift
//  muhamina
//
//  Created by Elsaadany on 6/8/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import UIKit

class SubMenuCell: UITableViewCell {

    //    MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with regulation: RegulationsDetails?) {
        self.titleLabel.text = regulation?.name ?? ""
        titleView.layoutIfNeeded()
        self.titleView.layer.cornerRadius = titleView.bounds.height / 2
        self.titleView.clipsToBounds = true
    }
}
