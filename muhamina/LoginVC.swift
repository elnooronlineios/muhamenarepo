//
//  TsgilElD5olViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/27/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import TextFieldEffects
import FCAlertView
import Alamofire
import PKHUD
import Spring
import Hero
import TransitionButton
//var user = userData()
class LoginVC: UIViewController, UITextFieldDelegate {
    var lawyerFlag = 0
    var showSignup = false
    @IBOutlet weak var password: HoshiTextField!
    @IBOutlet weak var email: HoshiTextField!
    var hideButton = false
    
    @IBOutlet weak var lawyerSignUp: UIButton!
    @IBAction func dimissView(_ sender: Any) {
        dismissing = 1
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet var img: UIImageView!

    @IBOutlet weak var sendMessageOutlet: TransitionButton!
    
    @IBAction func sendMessage(_ sender: Any) {
        login()
    }
    
    @IBAction func goToSignUp(_ sender: Any) {
//        let navVC = storyboard?.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
//        navigationController?.pushViewController(navVC, animated: true)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hero.isEnabled = true
        self.hero.isEnabled = true
        setupNavBar(vc: self)
        img.hero.id = "Ali"
        self.navigationController?.hero.navigationAnimationType = .fade
        
    }
    
    
    @IBOutlet weak var signUp_btn: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {
        
        if lawyerFlag == 1{
            signUp_btn.isHidden = true
            signUp_btn.isEnabled = false
            lawyerSignUp.isHidden = false
            lawyerSignUp.isEnabled = true
        }else{
            lawyerSignUp.isHidden = true
            lawyerSignUp.isEnabled = false
            signUp_btn.isHidden = false
            signUp_btn.isEnabled = true
        }
        if !isModal {
            self.navigationItem.leftBarButtonItem = nil
        }
        if hideButton {
            self.navigationItem.leftBarButtonItem = nil
        }
        if showSignup {
            let vc =  AppStoryboard.Main.viewController(viewControllerClass: Enshaa7sabViewController.self)
            showSignup = false
            self.show(vc, sender: self)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField.text == ""{
            let temp = textField as! HoshiTextField
            temp.borderActiveColor = UIColor.red
            temp.borderInactiveColor = UIColor.red
        }else{
            let temp = textField as! HoshiTextField
            //            temp.borderActiveColor = mainColor
            temp.borderInactiveColor = UIColor.gray
        }
        return false
    }
    
    
    
    
    
    func login(){
        sendMessageOutlet.startAnimation()
        if email.text!.isEmpty == false && password.text!.isEmpty == false {
            if !(email.text?.isValidEmail())!{
                makeDoneAlert(title: "البريد الإلكتروني غير صحيح", SubTitle: "من فضلك تأكد من إدخال البريد الإلكتروني بطريقة صحيحة", Image: #imageLiteral(resourceName: "icon13"))
                return
            }
            
            let header = [
                "Accept" : "application/json"
            ]
            let par = ["email": email.text!,
                       "password": password.text!,
                       "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid") ?? ""
                ] as [String:AnyObject]
            print(par)
            
            
            callApi(withURL: APIs.Instance.login(), method: .post, headers: APIs.Instance.getHeader(), Parameter: par) { (result) in
                
                switch result {
                case .success(let data):
                    do {
                        let user = try JSONDecoder().decode(SingleUserleData.self, from: data)
                        if user.data?.activated ?? "<null>" == "<null>" && user.data?.type ?? "" == "lawyer" {
                            makeDoneAlert(title: "تنبية", SubTitle: "سوف يتم تفعيل حسابك في أقرب وقت وأرسال تنبية بذلك", Image: #imageLiteral(resourceName: "icon13"))
                            }
                    UserDefaults.standard.set(data, forKey:"user")
                    UIApplication.shared.registerForRemoteNotifications()
                    UserDefaults.standard.set(false, forKey: "presentedCountryPicker")
                    User.shared.fetchUser()
                    NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier1"), object: nil)
                    self.dismissWithTransition()
                    }catch{
                        print(error.localizedDescription)
                    }
                default:
                    self.sendMessageOutlet.stopAnimation(animationStyle: .shake, revertAfterDelay: 0, completion: nil)
                }
            }
        }else{
            makeDoneAlert(title: "حقل فارغ", SubTitle: "من فضلك تأكد من إدخال جميع البيانات", Image: #imageLiteral(resourceName: "icon13"))
            if email.text!.isEmpty == true{
                email.borderActiveColor = UIColor.red
                email.borderInactiveColor = UIColor.red
            }
            if password.text!.isEmpty == true{
                password.borderActiveColor = UIColor.red
                password.borderInactiveColor = UIColor.red
            }
        }
    }
    
    func dismissWithTransition()  {
        self.sendMessageOutlet.stopAnimation(animationStyle: .expand, completion: {
            sleep(UInt32(0.1))
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didLogin"), object: nil, userInfo: nil)
            })
            
        })
    }
    
}
