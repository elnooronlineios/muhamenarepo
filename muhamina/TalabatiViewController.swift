//
//  TalabatiViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
class TalabatiViewController: UIViewController  , UITableViewDelegate , UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    var myOrders = Orders()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hero.isEnabled = true
        self.hero.isEnabled = true
//        self.view.viewWithTag(100)?.hero.id = "img02"
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        getMyOrders()
        configureTableView()
        tableView.separatorStyle = .none
    }
    
    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        tableView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0)
    }
    func getMyOrders(){
        HUD.show(.progress)
        callApi(withURL: APIs.Instance.getOrders(), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            HUD.hide()
            switch result {
            case .success(let data):
                print()
                do {
                  self.myOrders = try JSONDecoder().decode(Orders.self, from: data)
                    self.tableView.reloadData()
                }catch{
                    print(error)
                }
                
            default:
                break
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myOrders.data?.order_arr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TalabatiCell", for: indexPath)
        let item = (myOrders.data?.order_arr ?? [Order]())[indexPath.row]
        let nameLbl = cell.viewWithTag(1) as! UILabel
        let orderLbl = cell.viewWithTag(2) as! UILabel
        let phoneLbl = cell.viewWithTag(3) as! UILabel
        let locationLbl = cell.viewWithTag(4) as! UILabel
        let dateLbl = cell.viewWithTag(5) as! UILabel
        let timeLbl = cell.viewWithTag(6) as! UILabel
        let descLbl = cell.viewWithTag(8) as! UILabel
        let statLbl = cell.viewWithTag(9) as! UILabel
        let createdDate = cell.viewWithTag(11) as! UILabel
        let lbl = cell.viewWithTag(10) as! UILabel
        let imgView = cell.viewWithTag(12) as! UIImageView
        nameLbl.text = item.lawyer?.name ?? ""
        orderLbl.text = "\(item.id ?? 0)"
        phoneLbl.text = item.lawyer?.mobile ?? ""
        locationLbl.text = item.lawyer?.city?.name ?? ""
        let formatter = DateFormatter()
        if item.date == nil {
            cell.viewWithTag(7)?.isHidden = true
        }else{
            cell.viewWithTag(7)?.isHidden = false
            let date = item.date?.date?.convertToDate()
            formatter.dateFormat = "dd-MM-yyyy"
            dateLbl.text = item.date?.date?.toDate(format: "yyyy-MM-dd HH:mm:ss.zzzzzz")?.asString()
            formatter.dateFormat = "HH:mm"
            var time = item.date?.date?.toDate(format: "yyyy-MM-dd HH:mm:ss.zzzzzz")?.asString2().toDate(format: "HH:mm")
            timeLbl.text = time?.addingTimeInterval(-120.0 * 60.0).asString2()
        }
        let date = item.date?.date?.convertToDate()
        formatter.dateFormat = "dd-MM-yyyy"
        createdDate.text = formatter.string(from: date ?? Date())
        descLbl.text = item.details ?? ""
        statLbl.text = item.status ?? ""
        imgView.sd_setImage(with: URL(string: item.lawyer?.avatar ?? ""), completed: nil)
        statLbl.backgroundColor = #colorLiteral(red: 0, green: 0.3285208941, blue: 0.5748849511, alpha: 1)
        if statLbl.text == "pending" {
            statLbl.backgroundColor = #colorLiteral(red: 0.7215686275, green: 0.6745098039, blue: 0.2941176471, alpha: 1)
            statLbl.text = "قيد النظر"
        }
        if statLbl.text == "accepted"{
            statLbl.text = "مقبول"
            statLbl.backgroundColor = #colorLiteral(red: 0, green: 0.3285208941, blue: 0.5748849511, alpha: 1)
        }
        if statLbl.text == "rejected"{
            statLbl.text = "مرفوض"
            statLbl.backgroundColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        }
        
        nameLbl.hero.id = "\(indexPath.row)-1"
        imgView.hero.id = "\(indexPath.row)-3"
        nameLbl.changeFont()
        orderLbl.changeFont()
        phoneLbl.changeFont()
        locationLbl.changeFont()
        dateLbl.changeFont()
        timeLbl.changeFont()
        descLbl.changeFont()
        statLbl.changeFont()
        createdDate.changeFont()
        lbl.changeFont()
        cell.selectionStyle = .none
        cell.viewWithTag(14)?.isHidden = false
        cell.viewWithTag(13)?.isHidden = false
        if indexPath.row == 0 {
            cell.viewWithTag(13)?.isHidden = true
        }
        if indexPath.row == (myOrders.data?.order_arr?.count ?? 0) - 1{
            cell.viewWithTag(14)?.isHidden = true
        }
        return cell
        
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "LawyersViewController") as! LawyersViewController
        navVC.heroId = "\(indexPath.row)"
        navVC.laywer = myOrders.data?.order_arr?[indexPath.row].lawyer ?? Lawyer()
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
