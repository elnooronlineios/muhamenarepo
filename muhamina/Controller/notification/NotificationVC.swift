//
//  NotificationVC.swift
//  muhamina
//
//  Created by ElNoorOnline on 12/2/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import Hero

class NotificationVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var user_id = 0
     var notification_model = NotificationsModel()
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var notification_table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = .none
        
        configureTableView()
        getNotifications()
 
    }
    
    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notification_model.data?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ElRaeseyaCell", for: indexPath)
        let namelbl = cell.viewWithTag(1) as! UILabel
        let cityLbl = cell.viewWithTag(2) as! UILabel
        let imgView = cell.viewWithTag(3) as! UIImageView
        cell.selectionStyle = .none
        namelbl.hero.id = "\(indexPath.row)-1"
        cityLbl.hero.id = "\(indexPath.row)-2"
        imgView.hero.id = "\(indexPath.row)-3"
        cell.viewWithTag(4)?.hero.id = "\(indexPath.row)-4"
        print(namelbl.hero.id)
        let item = notification_model.data?[indexPath.row]
        namelbl.text = item?.title ?? ""
        cityLbl.text = item?.body ?? ""
        imgView.sd_setImage(with: URL(string:item?.image ?? ""), completed: nil)
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "TalabatiViewController") as! TalabatiViewController
       
        
        if user_id == 0 {
            self.navigationController?.pushViewController(navVC, animated: true)

        }else {
            
            notifications_details((notification_model.data?[indexPath.row].data?.appointment_id)!)
            
        }
    }
    
    
    func getNotifications(){
        
         HUD.show(.progress)
        callApi(withURL: APIs.Instance.notification(), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            
            HUD.hide()
            
            switch result {
            case .success(let data):
                print(data)
                do {
                    
                    self.notification_model = try JSONDecoder().decode(NotificationsModel.self, from: data)
                    self.notification_table.reloadData()
                }catch{
                    print(error)
                }
                
            default:
                break
            }
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }
    
    
    /* if lawyer */
    func notifications_details(_ id : Int){
        
        HUD.show(.progress)
        callApi(withURL: APIs.Instance.appointments_details(id), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            
            HUD.hide()
            
            switch result {
            case .success(let data):
                print(data)
                do {
                    
 
                    let orders_model = try JSONDecoder().decode(Orders.self, from: data)
                    
                   let vc = AppStoryboard.Main.viewController(viewControllerClass: Tafasil3nElTalabViewController.self)

                    vc.hero.isEnabled = true
//                    vc.heroId = "\(indexPath.row)"
                   self.navigationController?.hero.navigationAnimationType = .auto
                    vc.order = (orders_model.data?.order_obj)!
                   self.navigationController?.pushViewController(vc, animated: true)
                    
                }catch{
                    print(error)
                }
                
            default:
                break
            }
        }
        
    }
    

}
