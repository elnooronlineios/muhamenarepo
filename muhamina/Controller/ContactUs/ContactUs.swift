//
//  ContactUs.swift
//  muhamina
//
//  Created by ElNoorOnline on 12/2/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD

class ContactUs: UIViewController {

    
    
    @IBOutlet weak var message_txtField: UITextView!
    
    @IBAction func send_btn(_ sender: Any) {
        
        if message_txtField.text!.isEmpty == false  {
            
            contact_us()
        }else{
            makeDoneAlert(title: "", SubTitle: "من فضلك املء البيانات", Image: #imageLiteral(resourceName: "icon13"))
        }
        
        
        
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()

       setupNavBar(vc: self)
        self.navigationItem.title = "تواصل معنا"
 
    }
    func contact_us(){
        
        HUD.show(.progress)
        let par = ["message": message_txtField.text!] as [String:AnyObject]
        print(par)
        
        
        callApi(withURL: APIs.Instance.contact_us(), method: .post, headers: APIs.Instance.getHeader(), Parameter: par) { (result) in
            HUD.hide()
            switch result {
            case .success(let data):
                self.message_txtField.text = ""
                self.success_message()
            default:
                print("-- error")
            }
        }
        
    }
    
    func success_message(){
        let vc = AppStoryboard.Main.viewController(viewControllerClass: TmEl2rsalViewController.self)
        vc.orderNumber =  0
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        self.present(vc, animated: true, completion: nil)
        
    }
    

}
