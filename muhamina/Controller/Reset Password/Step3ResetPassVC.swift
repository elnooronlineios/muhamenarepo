//
//  Step3ResetPassVC.swift
//  muhamina
//
//  Created by Esraa Masuad on 11/21/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import TextFieldEffects
import FCAlertView
import Alamofire
import PKHUD
import Spring
import Hero
import TransitionButton

class Step3ResetPassVC: UIViewController {

    var user_token = String()
    
    @IBOutlet weak var pass_txtField: HoshiTextField!
    @IBOutlet weak var confirmPass_txtField: HoshiTextField!

    @IBAction func send_btn(_ sender: Any) {
    
        if pass_txtField.text!.isEmpty == false && confirmPass_txtField.text!.isEmpty == false {
        
            
            if pass_txtField.text! != confirmPass_txtField.text!{
                makeDoneAlert(title: "", SubTitle: " كلمتا المرور غير متطابقة", Image: #imageLiteral(resourceName: "icon13"))
                return
            }
            
            forgetPass()
            
            
        }else{
            makeDoneAlert(title: "حقل فارغ", SubTitle: "من فضلك تأكد من إدخال جميع البيانات", Image: #imageLiteral(resourceName: "icon13"))
            if pass_txtField.text!.isEmpty == true{
                pass_txtField.borderActiveColor = UIColor.red
                pass_txtField.borderInactiveColor = UIColor.red
            }
            if confirmPass_txtField.text!.isEmpty == true{
                confirmPass_txtField.borderActiveColor = UIColor.red
                confirmPass_txtField.borderInactiveColor = UIColor.red
            }
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setupNavBar(vc: self)
      
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    
    func forgetPass(){
        
        HUD.show(.progress)
        let header = [
            "Accept" : "application/json"
        ]
        let par = ["password": pass_txtField.text!,
                   "password_confirmation" : confirmPass_txtField.text!,
                   "token" : user_token] as [String:AnyObject]
        print(par)
        
        
        callApi(withURL: APIs.Instance.resetPassword(), method: .post, headers: header, Parameter: par) { (result) in            
            HUD.hide()

            switch result {
            case .success( _):
                self.goto()
             default:
                print("-- error")
            }
        }
        
    }
    
    
    func goto(){
        makeDoneAlert(title: "", SubTitle: "تم اعادة تعيين كلمة المرور بنجاح", Image: #imageLiteral(resourceName: "img08"))

        let VC = storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        navigationController?.pushViewController(VC, animated: true)
    }
 

}
