//
//  Step2CheckCodeVC.swift
//  muhamina
//
//  Created by Esraa Masuad on 11/21/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import TextFieldEffects
import FCAlertView
import Alamofire
import PKHUD
import Spring
import Hero
import TransitionButton

class Step2CheckCodeVC: UIViewController {


    //var user_mail:((_ mail: String ) -> ())?
    
    var user_mail = String()
    var user_token: String = ""

    @IBOutlet weak var code_txtField: HoshiTextField!

    @IBAction func send_btn(_ sender: Any) {
    
//goto()
        if code_txtField.text!.isEmpty == false  {

            forgetPass()


        }else{
            makeDoneAlert(title: "حقل فارغ", SubTitle: "من فضلك تأكد من إدخال جميع البيانات", Image: #imageLiteral(resourceName: "icon13"))

                code_txtField.borderActiveColor = UIColor.red
                code_txtField.borderInactiveColor = UIColor.red


        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavBar(vc: self)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
    //    setNavigation()
        
        
    }
    
    
    
    func forgetPass(){
        
        HUD.show(.progress)
        let header = [
            "Accept" : "application/json"
        ]
        let par = ["email": user_mail,
                   "code" : code_txtField.text!] as [String:AnyObject]
        print(par)
        
        
        callApi(withURL: APIs.Instance.checkCode(), method: .post, headers: header, Parameter: par) { (result) in
            
            HUD.hide()
            switch result {
            case .success(let data):
                
                do {
                    
                    let token_data = try JSONDecoder().decode(ResetPassword.self, from: data)
                    self.user_token = token_data.token!
                     self.goto()
                }catch{
                    print(error)
                }

            default:
                print("-- error")
            }
        }
        
    }
    
    
    func goto(){
        let VC = storyboard?.instantiateViewController(withIdentifier: "resetPassVC") as! Step3ResetPassVC
        
        VC.user_token = user_token
        navigationController?.pushViewController(VC, animated: true)
    }
    
    func setNavigation(){
        
     setupNavBar(vc: self)
    self.navigationItem.title = " تغير كلمة المرور"
        
    }

   
    

}
