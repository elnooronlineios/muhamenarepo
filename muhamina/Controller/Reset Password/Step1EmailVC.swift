//
//  Step1EmailVC.swift
//  muhamina
//
//  Created by Esraa Masuad on 11/21/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

import TextFieldEffects
import FCAlertView
import Alamofire
import PKHUD
import Spring
import Hero
import TransitionButton

class Step1EmailVC: UIViewController {

    
    @IBOutlet weak var email_txtField: HoshiTextField!
    
    @IBAction func send_btn(_ sender: Any) {
        
    //    goto()
        if email_txtField.text!.isEmpty == false  {
            if !(email_txtField.text?.isValidEmail())!{
                makeDoneAlert(title: "البريد الإلكتروني غير صحيح", SubTitle: "من فضلك تأكد من إدخال البريد الإلكتروني بطريقة صحيحة", Image: #imageLiteral(resourceName: "icon13"))
                return
            }

            forgetPass()


        }else{
            makeDoneAlert(title: "حقل فارغ", SubTitle: "من فضلك تأكد من إدخال جميع البيانات", Image: #imageLiteral(resourceName: "icon13"))

                email_txtField.borderActiveColor = UIColor.red
                email_txtField.borderInactiveColor = UIColor.red


        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavBar(vc: self)
       
    }

   
    func forgetPass(){
        
        HUD.show(.progress)
            let header = [
                "Accept" : "application/json"
            ]
            let par = ["email": email_txtField.text!] as [String:AnyObject]
            print(par)
            
            
            callApi(withURL: APIs.Instance.forgetPassword(), method: .post, headers: header, Parameter: par) { (result) in
                HUD.hide()
                switch result {
                case .success(let data):
                     
                    self.goto()
                default:
                    print("-- error")
                }
            }
        
    }
    
    
    func goto(){
        
        makeDoneAlert(title: "", SubTitle: "تم ارسال كود الي بريدك الاليكتروني ينتهي خلال ١٠ دقائق", Image: #imageLiteral(resourceName: "img08"))

        let VC = storyboard?.instantiateViewController(withIdentifier: "checkCodeVC") as! Step2CheckCodeVC
        VC.user_mail = email_txtField.text!
        navigationController?.pushViewController(VC, animated: true)
        
    }
    
    
        
    
    

}
