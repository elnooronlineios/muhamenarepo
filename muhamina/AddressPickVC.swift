//
//  AddressPickVC.swift
//  muhamina
//
//  Created by Mohamed Nawar on 5/13/20.
//  Copyright © 2020 MuhammedAli. All rights reserved.
//


    import UIKit
    import GoogleMaps
    import FCAlertView
    import Alamofire
    import PKHUD

protocol ShareLocationProtocol: AnyObject {
    func share(_ location: SelectedLocation) -> ()
}

    class AddressPickVC: UIViewController {
        
        private let locationManager = CLLocationManager()
        
        var userProfile = User.shared
        var myLocation = CLLocation()
        let marker = GMSMarker()
        var location = SelectedLocation()
        weak var delegate: ShareLocationProtocol?
        
        @IBOutlet weak var mapView: GMSMapView!
        @IBOutlet weak var locationLabel: UILabel!
        

        override func viewDidLoad() {
            super.viewDidLoad()
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            createMap()
            User.shared.fetchUser()
        }
        
        @IBAction func doneTapped(_ sender: UIButton) {
//            if  userProfile.data?.country?.name?.lowercased() ?? "" == location.country?.lowercased() ?? "" {
                delegate?.share(location)
                self.dismiss(animated: true, completion: nil)
        }
        
        
        func createMap() {
            let camera = GMSCameraPosition.camera(withLatitude: 29.3117, longitude: 47.4818, zoom: 10.0)
            mapView.camera = camera
            mapView.settings.myLocationButton = true
            reverseGeocoding(marker: marker)
            marker.map = mapView
            self.mapView.delegate = self
        }
        
        

    }
    //MARK:- GMSMapViewDelegate
    extension AddressPickVC: GMSMapViewDelegate {
        
        func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
    //        updateAddressFromCurrentLocation()
            let camera = GMSCameraPosition(target: self.myLocation.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.mapView?.animate(to: camera)
            return true
        }
        
        func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
            marker.position = position.target
            reverseGeocoding(marker: marker)
            
        }
        
        
        //Mark: Reverse GeoCoding
        
        func reverseGeocoding(marker: GMSMarker) {
            let geocoder = GMSGeocoder()
            geocoder.accessibilityLanguage = "ar"
            let coordinate = CLLocationCoordinate2DMake(Double(marker.position.latitude),Double(marker.position.longitude))
            
            var currentAddress = String()
            
            geocoder.reverseGeocodeCoordinate(coordinate) { response , error in
                if let address = response?.firstResult() {
                    let lines = address.lines! as [String]
                    print(response?.firstResult()?.country?.lowercased())
                    
                    currentAddress = lines.joined(separator: ", ")
                    self.locationLabel.text = currentAddress
                    self.location.address = currentAddress
                    self.location.longitude = "\(marker.position.longitude)"
                    self.location.latitude = "\(marker.position.latitude)"
                    self.location.country = response?.firstResult()?.country
                    
                }
                marker.title = currentAddress
                marker.map = self.mapView
            }
        }
    }

    // MARK: - CLLocationManagerDelegate
    //1
    extension AddressPickVC: CLLocationManagerDelegate {
        // 2
        func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            
            // 3
            guard status == .authorizedWhenInUse else {return}
            
            // 4
            locationManager.startUpdatingLocation()
            
            //5
            mapView?.isMyLocationEnabled = true
            mapView?.settings.myLocationButton = true
            
        }
        
        //6
        func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            
            guard let location = locations.first else {return}
            self.myLocation = location
            
            // 7
            let camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
            self.mapView?.animate(to: camera)
            
            // 8
            locationManager.stopUpdatingLocation()
        }
        
        
    }



struct SelectedLocation {
    var country: String?
    var address: String?
    var longitude: String?
    var latitude: String?
}
