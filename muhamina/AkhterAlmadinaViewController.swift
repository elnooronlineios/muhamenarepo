//
//  AkhterAlmadinaViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import Alamofire
class AkhterAlmadinaViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    let picker = UIPickerView()
    var cities = CityMain()
    var selectedCountry = 0
    @IBOutlet var pickCityField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar(vc: self)
        getCities()
//        if !User.shared.isLoggedIn() {
            UserDefaults.standard.set(true, forKey: "presentedCountryPicker")
//
//        }
    }
    @IBAction func showResults(_ sender: Any) {
        UserDefaults.standard.set(selectedCountry, forKey: "countryId")
        didPick = true
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadLawyers"), object: nil, userInfo: ["id":self.selectedCountry,"name":self.pickCityField.text ?? ""])
        }

        
    }
    func getCities(){
            HUD.show(.progress)
        print(APIs.Instance.getCities())
        callApi(withURL: APIs.Instance.getCities(), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            HUD.hide()
            switch result {
            case .success(let data):
                print(data)
                do {
                    self.cities = try JSONDecoder().decode(CityMain.self, from: data)
                    self.setupPicker()
                }catch{
                    
                }
                
            default:
                break
            }
        }

    }
    
    func setupPicker(){
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker));
        let cancelButton = UIBarButtonItem(title: "إلغاء", style: .plain, target: self, action: #selector(cancelPicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        pickCityField.inputView = picker
        pickCityField.inputAccessoryView = toolbar
    }
    @objc func donePicker(){
        if pickCityField.text == "" {
            if (cities.data ?? [CityData]()).count > 0 {
                pickCityField.text = cities.data?[0].name ?? ""
                selectedCountry = cities.data?[0].id ?? -1
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
        self.view.endEditing(true)
        
    }
    @objc func cancelPicker(){
        pickCityField.text = ""
        self.view.endEditing(true)
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return cities.data?[row].name ?? ""
        
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickCityField.text = cities.data?[row].name ?? ""
        selectedCountry = cities.data?[row].id ?? -1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (cities.data ?? [CityData]()).count
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    
}
