//
//  Extensions.swift
//  Rafeeq
//
//  Created by mouhammed ali on 9/22/17.
//  Copyright © 2017 mouhammed ali. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import PKHUD
import FCAlertView

extension UIViewController{
    var isModal: Bool {
        if let index = navigationController?.viewControllers.index(of: self), index > 0 {
            return false
        } else if presentingViewController != nil {
            return true
        } else if navigationController?.presentingViewController?.presentedViewController == navigationController  {
            return true
        } else if tabBarController?.presentingViewController is UITabBarController {
            return true
        } else {
            return false
        }
    }
}
public extension UILabel {
    public func changeFont(){
        self.font = UIFont(name: "NeoSansArabic", size: self.font.pointSize)
    }
}
public func makeDoneAlert(title: String, SubTitle: String, Image : UIImage) {
    let alert = FCAlertView()
    alert.avoidCustomImageTint = true
    alert.colorScheme = #colorLiteral(red: 0.4914782643, green: 0.1862874627, blue: 0.7936523557, alpha: 1)
    alert.showAlert(withTitle: title, withSubtitle: SubTitle, withCustomImage: Image, withDoneButtonTitle: "تم", andButtons: nil)
}
public enum ConnectionResult {
    case success(Data)
    case failure(Error)
    case error()
}
public extension UIWindow {
    
    /** @return Returns the current Top Most ViewController in hierarchy.   */
    public func topMostVC()->UIViewController? {
        
        var topController = rootViewController
        
        while let presentedController = topController?.presentedViewController {
            topController = presentedController
        }
        
        return topController
    }
    
    /** @return Returns the topViewController in stack of topMostController.    */
    public func currentViewController()->UIViewController? {
        
        var currentViewController = topMostVC()
        
        while currentViewController != nil && currentViewController is UINavigationController && (currentViewController as! UINavigationController).topViewController != nil {
            currentViewController = (currentViewController as! UINavigationController).topViewController
        }
        
        return currentViewController
    }
}

public func callApi(withURL:String, method:HTTPMethod, headers:[String:String], Parameter:[String:Any],completionHandler:@escaping (_ success:ConnectionResult) -> Void) {
    Alamofire.request(withURL, method: method, parameters: Parameter, encoding: URLEncoding.default, headers: headers).responseData {
        (response:DataResponse) in
        print(response.value)
        switch(response.result) {
        case .success(_):
            
            if APIs.Instance.login() == withURL || APIs.Instance.getOrders() == withURL {
                print(APIs.Instance.getOrders())
                print(headers)
                print(response.value)
            }
             print(response.value)
            let temp = response.response?.statusCode ?? 400
            if temp >= 300 {
            do {
                //Fail
                let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                completionHandler(.error())
                if err.parseError() == "" {
                   
                }
                makeErrorAlert(subTitle: "\(err.parseError())")
                
            }catch{
                completionHandler(.error())
                makeErrorAlert(subTitle:"حدث خطأ برجاء اعادة المحاولة")
                
            }
            
        }else{
                completionHandler(.success(response.data ?? Data()))
        }
            break
            
        case .failure(let error):
            completionHandler(.failure(error))
            print(response.result.error)
            
        }
    }
    //
}
public func setupNavBar(vc:UIViewController){
    
    vc.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    vc.navigationController?.navigationBar.shadowImage = UIImage()
    vc.navigationController?.navigationBar.isTranslucent = true
    vc.navigationController?.navigationBar.titleTextAttributes =
        [NSAttributedStringKey.font:  UIFont(name: "NeoSansArabic", size: 16)! ,
         NSAttributedStringKey.foregroundColor: UIColor.white
    ]
    vc.navigationController?.navigationBar.tintColor = .white
    
}
extension UIView {
    func applyNavBarConstraints(size: (width: CGFloat, height: CGFloat)) {
        let widthConstraint = self.widthAnchor.constraint(equalToConstant: size.width)
        let heightConstraint = self.heightAnchor.constraint(equalToConstant: size.height)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
    }
}
extension String {
    func isValidEmail() -> Bool {
        // here, `try!` will always succeed because the pattern is valid
        let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let firstMatch = dataDetector?.firstMatch(in: self, options: NSRegularExpression.MatchingOptions.reportCompletion, range: NSRange(location: 0, length: length))
        return (firstMatch?.range.location != NSNotFound && firstMatch?.url?.scheme == "mailto")
    }
    func convertToDate()->Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        let date = dateFormatter.date(from:self) ?? Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date)
        let finalDate = calendar.date(from:components)
        return finalDate ?? Date()
    }
    public var length: Int {
        return self.characters.count
    }
}

@IBDesignable extension UIView {
    
    
    @IBInspectable var shadow: Bool {
        get {
            return layer.shadowOpacity > 0.0
        }
        set {
            if newValue == true {
                self.addShadow()
            }
        }
    }
    
    
    
    
    
    
    func addShadow(shadowColor: CGColor = UIColor.black.cgColor,
                   shadowOffset: CGSize = CGSize(width: 0, height: 3.0),
                   shadowOpacity: Float = 0.2,
                   shadowRadius: CGFloat = 5.0) {
        layer.shadowColor = shadowColor
        layer.shadowOffset = shadowOffset
        layer.shadowOpacity = shadowOpacity
        layer.shadowRadius = shadowRadius
        layer.masksToBounds = false
        
    }
}
extension UIImage {
    func base64String() -> String{
        let imageData: NSData = UIImageJPEGRepresentation(self, 0.1)! as NSData
        let str = imageData.base64EncodedString(options: .lineLength64Characters)
        return str
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    public var replacedArabicDigitsWithEnglish: String {
        var str = self
        let map = ["٠": "0",
                   "١": "1",
                   "٢": "2",
                   "٣": "3",
                   "٤": "4",
                   "٥": "5",
                   "٦": "6",
                   "٧": "7",
                   "٨": "8",
                   "٩": "9",
                   "،":".",
                   ".":".",
                   ",":"."]
        map.forEach { str = str.replacingOccurrences(of: $0, with: $1) }
        return str
        
    }
}
extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "\(self)", comment: "")
    }
}


/// The UITextView placeholder text

@IBDesignable extension UIView {
    @IBInspectable var blink: Bool {
        set {
            if newValue {
                //                Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { timer in
                //                    UIView.animate(withDuration: 0.5/*Animation Duration second*/, animations: {
                //                        if self.alpha == 0{
                //                            self.alpha = 1}else{
                //                            self.alpha = 0
                //                        }
                //                    }, completion:  {
                //                        (value: Bool) in
                //                        if self.alpha == 0{
                //                         //   self.isHidden = true
                //
                //                        }else{
                //                           // self.isHidden = false
                //                        }
                //
                //
                //                    })
                //                }
                
                //                self.alpha = 0.0;
                //                UIView.animate(withDuration: 0.8, //Time duration you want,
                //                    delay: 0.0,
                //                    options: [.curveEaseInOut, .autoreverse, .repeat],
                //                    animations: { [weak self] in self?.alpha = 1.0 },
                //                    completion:nil)
            }else{
                layer.removeAllAnimations()
                alpha = 1
            }
        }
        get {
            return self.blink
        }
    }
}

extension UIView {
    func setRadius(for corners: UIRectCorner, with radius:Int ) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        self.setNeedsLayout()
    }
}

extension CGRect {
    
    mutating func addHeight(h:CGFloat) {
        
        let yPosition = self.origin.y
        let x = self.origin.x
        let height = self.height
        let width = self.width
        self = CGRect(x: x, y: yPosition, width: width, height: height+h)
        
        
    }
    mutating func setHeight(h:CGFloat) {
        
        let yPosition = self.origin.y
        let x = self.origin.x
        _ = self.height
        let width = self.width
        self = CGRect(x: x, y: yPosition, width: width, height: h)
        
        
    }
    mutating func addX(x:CGFloat) {
        var frame:CGRect = self
        frame.origin.x = x + frame.origin.x
        self = frame
    }
    mutating func setX(x:CGFloat) {
        var frame:CGRect = self
        frame.origin.x = x
        self = frame
    }
    mutating func setY(y:CGFloat) {
        var frame:CGRect = self
        frame.origin.y = y
        self = frame
    }
    mutating func addY(y:CGFloat) {
        var frame:CGRect = self
        frame.origin.y = y + frame.origin.y
        self = frame
    }
}

public func makeErrorAlert(subTitle: String) {
    let alert = FCAlertView()
    alert.makeAlertTypeWarning()
    alert.colorScheme = ColorControl.sharedInstance.getAlertColor()
    alert.showAlert(withTitle: "خطأ", withSubtitle: subTitle, withCustomImage: nil, withDoneButtonTitle: "حسناً", andButtons: nil)
}
/*extension UILabel {
 
 func startBlink() {
 UIView.animate(withDuration: 0.8,
 delay:0.0,
 options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
 animations: { self.alpha = 0 },
 completion: nil)
 }
 
 func stopBlink() {
 layer.removeAllAnimations()
 alpha = 1
 }
 }*/
