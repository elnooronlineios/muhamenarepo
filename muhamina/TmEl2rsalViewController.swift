//
//  TmEl2rsalViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/27/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class TmEl2rsalViewController: UIViewController {
    var orderNumber = 0
    
    
    @IBOutlet var orderNumberLbl: UILabel!
    
    @IBOutlet weak var message_txt: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if orderNumber != 0{
            orderNumberLbl.text = "رقم الطلب هو \(orderNumber)"
        }else{
            orderNumberLbl.text = ""
            message_txt.text = "تم ارسال الرسالة بنجاح "
            
        }
    }

  
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
