//
//  AppDelegate.swift
//  muhamina
//
//  Created by MACBOOK on 7/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import IQKeyboardManager
import OneSignal
import GoogleMaps
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,OSSubscriptionObserver {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
          GMSServices.provideAPIKey("AIzaSyB5bqJ6WM0DJcdmNdHnVc_ZQRO2GVb9AV4")
        ////Start One Signal Initializtion////
//        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFontTextStyle(rawValue: "NeoSansArabic"))
        UITabBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().tintColor = .white
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: "3995c19f-b798-423f-85cf-ccfe5fa41aef",
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        
        // Recommend moving the below line to prompt for push after informing the user about
        //   how your app will use them.
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        OneSignal.add(self as OSSubscriptionObserver)
        ////End One Signal Initializtion////
        
        
        IQKeyboardManager.shared().isEnabled = true
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(-1000, 0), for:UIBarMetrics.default)
        UIFont.familyNames.forEach({ familyName in
            let fontNames = UIFont.fontNames(forFamilyName: familyName)
            print(familyName, fontNames)
        })
//        MARK:- Root
//        UINavigationBar.appearance().semanticContentAttribute = true ? .forceRightToLeft : .forceLeftToRight
//        let appDelegate = UIApplication.shared.delegate as? AppDelegate
//            let vc = AppStoryboard.Menus.viewController(viewControllerClass: MenuVC.self)
//        appDelegate?.window?.rootViewController = vc
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
        
        //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
        if let playerId = stateChanges.to.userId {
            
            UserDefaults.standard.set(playerId, forKey: "onesignalid")
            print(UserDefaults.standard.string(forKey: "onesignalid"))
            print("Current playerId \(playerId)")
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "addOneSignalIdNC"), object: nil, userInfo: nil)
            
        }
    }

    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        

        completionHandler(.noData);
        print(userInfo)
        
        if ( application.applicationState == .inactive || application.applicationState == .background || application.applicationState == .active){
            
        if let userInfoDict = userInfo["custom"] as? [String:AnyObject] {
            
            if let userInfoA = userInfoDict["a"] as? [String:AnyObject]{
                
                if let type = userInfoA["type"] as? String {
                    
                    switch type {
                    case "new_appointment_stored":
                    print(type)
                if let id = userInfoA["appointment_id"] as? Int {
                     let dicData: [String:String] = ["id": "\(id)", "type":"\(type)"]
                    NotificationCenter.default.post(name: Notification.Name("pressNotificationForLawyer"), object: nil, userInfo: dicData)
                        }
                    case "lawyer_activation_notification":
                     let dicData: [String:String] = ["id": "", "type":"\(type)"]
                    NotificationCenter.default.post(name: Notification.Name("pressNotificationForLawyer"), object: nil, userInfo: dicData)
                    case "lawyer_activated_notification":
                         let dicData: [String:String] = ["id": "", "type":"\(type)"]
                        NotificationCenter.default.post(name: Notification.Name("pressNotificationForLawyer"), object: nil, userInfo: dicData)
                    case "appointment_status_notification":
                    print(type)
                    if let userInfoDict = userInfo["aps"] as? [String:AnyObject] {
                        if let userInfoA = userInfoDict["alert"] as? [String:AnyObject]{
                if let body = userInfoA["body"] as? String,  let title = userInfoA["title"] as? String {
                    let dicData: [String:String] = ["title": title , "body": body]
                        NotificationCenter.default.post(name: Notification.Name("pressNotificationForUser"), object: nil, userInfo: dicData)
                        }
                    }
                }
                    case "appointment_date_notification":
                    print(type)
                        if let userInfoDict = userInfo["aps"] as? [String:AnyObject] {
                            if let userInfoA = userInfoDict["alert"] as? [String:AnyObject]{
                    if let body = userInfoA["body"] as? String,  let title = userInfoA["title"] as? String {
                        let dicData: [String:String] = ["title": title , "body": body]
                            NotificationCenter.default.post(name: Notification.Name("pressNotificationForUser"), object: nil, userInfo: dicData)
                            }
                        }
                    }
                    default:
                        return
                    }
                    
                }
                
            }
            
            }
            
        }
        
    }
}

