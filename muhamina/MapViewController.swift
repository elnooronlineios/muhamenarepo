//
//  MapViewController.swift
//  muhamina
//
//  Created by iMac on 10/30/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.

import UIKit
import GoogleMaps
import CoreLocation
class MapViewController: UIViewController , CLLocationManagerDelegate {
    @IBOutlet weak var mapView: GMSMapView!
    let locationManager = CLLocationManager()
    var imgView = UIImageView()
    var lawyerName = String()
    var lawyerCoordinate = CLLocationCoordinate2D(){
        didSet{
            let marker = GMSMarker()
            marker.position = lawyerCoordinate
            marker.title = lawyerName
            marker.snippet = lawyerName
            marker.icon = imgView.image
            marker.map = self.mapView
            let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: 17.0)
            
            self.mapView?.animate(to: camera)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        GMSServices.provideAPIKey("AIzaSyAcTqtg8D77LF88xx_JWm_AEsxTel3n94g")
        locationManager.requestAlwaysAuthorization()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.clear]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        self.navigationController?.navigationBar.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
    }
    var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if  let coordinate = locationManager.location?.coordinate {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: (coordinate.latitude), longitude: (coordinate.longitude))
            marker.title = "title"
            marker.snippet = "snippet"
            marker.icon = UIImage(named: "centerMapBtn")
            marker.map = self.mapView
            let camera = GMSCameraPosition.camera(withLatitude: (coordinate.latitude), longitude: (coordinate.longitude), zoom: 17.0)
            
            self.mapView?.animate(to: camera)
            
            //Finally stop updating location otherwise it will come again and again in this delegate
            self.locationManager.stopUpdatingLocation()
        }else {
        }
    }
}
