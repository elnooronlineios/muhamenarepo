//
//  AlartForLoginViewController.swift
//  muhamina
//
//  Created by iMac on 10/30/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class AlartForLoginViewController: UIViewController {
    var backTo = String()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func registerationBtn(_ sender: Any) {
        self.dismiss(animated: true){
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.present(navVC, animated: true)
        }
        
    }
    
    @IBAction func loginBtn(_ sender: Any) {
        self.dismiss(animated: true){
            let navVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.navigationController?.present(navVC, animated: true)
        }
        
    }
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
