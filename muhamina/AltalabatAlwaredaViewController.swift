//
//  AltalabatAlwaredaViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
class AltalabatAlwaredaViewController: UIViewController , UITableViewDelegate , UITableViewDataSource {
   var myOrders = Orders(){
        didSet{
            if receiveAppointmentWithNotification {
                moveToReceivedAppointmentWithNotification()
            }
        }
    }
   var receiveAppointmentWithNotification = false
    
    func moveToReceivedAppointmentWithNotification() {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: Tafasil3nElTalabViewController.self)
                               vc.hero.isEnabled = true
                               vc.heroId = "0"
                               self.navigationController?.hero.navigationAnimationType = .auto
                               vc.order = (myOrders.data?.order_arr ?? [Order]())[0]
                               self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        configureTableView()
        tableView.separatorStyle = .none
        getMyOrders()
        self.hero.isEnabled = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("pressNotificationForLawyer"), object: nil)
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
    
        if let type = notification.userInfo?["type"] as? AnyObject {
            switch "\(type)" {
            case "lawyer_activated_notification":
                makeDoneAlert(title: "تنبية", SubTitle: "لقد تم تفعيل حسابك", Image: #imageLiteral(resourceName: "icon13"))
            case "lawyer_activation_notification":
                makeDoneAlert(title: "تنبية", SubTitle: "لقد تم تفعيل حسابك", Image: #imageLiteral(resourceName: "icon13"))
            case "new_appointment_stored":
                if let id = notification.userInfo?["id"] as? String {
                       getMyOrders()
                    receiveAppointmentWithNotification = true
                     }
            default:
                return
            }
        }
     }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        receiveAppointmentWithNotification = false
    }
    
    @IBAction func notification_icon(_ sender: Any) {
        let navVC = storyboard?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        
        navVC.user_id = -1
        self.navigationController?.hero.navigationAnimationType = .auto
        self.navigationController?.pushViewController(navVC, animated: true)
    }
    
    
    func configureTableView() {
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
        tableView.contentInset = UIEdgeInsetsMake(100, 0, 0, 0)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: Tafasil3nElTalabViewController.self)
        vc.hero.isEnabled = true
        vc.heroId = "\(indexPath.row)"
        self.navigationController?.hero.navigationAnimationType = .auto
        vc.order = (myOrders.data?.order_arr ?? [Order]())[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myOrders.data?.order_arr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AltalabatAlwaredaCell", for: indexPath)
        let item = (myOrders.data?.order_arr ?? [Order]())[indexPath.row]
        let nameLbl = cell.viewWithTag(1) as! UILabel
        let orderLbl = cell.viewWithTag(2) as! UILabel
        let orderNumberLbl = cell.viewWithTag(3) as! UILabel
        let phoneLbl = cell.viewWithTag(4) as! UILabel
        let imgView = cell.viewWithTag(5) as! UIImageView
        let dateLbl1 = cell.viewWithTag(6) as! UILabel
        let dateLbl2 = cell.viewWithTag(7) as! UILabel
        
        nameLbl.hero.id = "\(indexPath.row)-1"
        orderLbl.hero.id = "\(indexPath.row)-2"
        imgView.hero.id = "\(indexPath.row)-3"
        cell.viewWithTag(8)?.hero.id = "\(indexPath.row)-4"
        cell.viewWithTag(9)?.hero.id = "\(indexPath.row)-5"
        
        nameLbl.text = item.client?.name ?? ""
        orderLbl.text = item.title ?? ""
        phoneLbl.text = item.client?.mobile ?? ""
        orderNumberLbl.text = "\(item.id ?? 0)"
        
        /* render date */
        let date_str = item.created_at?.date
        print("\(date_str!)")
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss.zzzzzz"
        //dateFormatterGet.locale = Locale(identifier: "ar")
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.locale = Locale(identifier: "ar")
        dateFormatterPrint.dateFormat = "dd MMMM"
        
        if let date = dateFormatterGet.date(from: date_str!) {
            
            dateLbl1.text = dateFormatterPrint.string(from: date)
            
            dateFormatterPrint.dateFormat = "yyyy"
            dateLbl2.text = dateFormatterPrint.string(from: date)
          //  print(dateFormatterPrint.string(from: date))
        } else {
            print("There was an error decoding the string")
        }
        
//        let date = item.created_at?.date?.convertToDate()
//        let formatter = DateFormatter()
//        formatter.locale = Locale(identifier: "ar")
//        formatter.dateFormat = "dd MMMM"
//        dateLbl1.text = formatter.string(from: date ?? Date())
//        formatter.dateFormat = "yyyy"
//        dateLbl2.text = formatter.string(from: date ?? Date())
        
        imgView.sd_setImage(with: URL(string: item.client?.image ?? ""), completed: nil)
        cell.selectionStyle = .none
        return cell
        
    }
    
    func getMyOrders(){
        HUD.show(.progress)
      
        callApi(withURL: APIs.Instance.getOrders(), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            HUD.hide()
            switch result {
            case .success(let data):
                print()
                do {
                    self.myOrders = try JSONDecoder().decode(Orders.self, from: data)
                    self.tableView.reloadData()
                }catch{
                    print(error)
                }
                
            default:
                break
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }

}
extension Date {
    func asString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return dateFormatter.string(from: self)
    }
    func asString2() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        return dateFormatter.string(from: self)
    }
}
extension String {
    
    func toDate(format:String)-> Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: self)
        
        return date
        
    }
}
