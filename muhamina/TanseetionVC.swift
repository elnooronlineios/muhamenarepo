//
//  TanseetionVC.swift
//  muhamina
//
//  Created by mouhammed ali on 8/19/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class TanseetionVC: UIViewController {
    let dismissButton = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap)))
        dismissButton.imageView?.contentMode = .scaleAspectFit
        dismissButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        dismissButton.setImage(UIImage(named: "back"), for: .normal)
        dismissButton.hero.id = "back button"
        view.addSubview(dismissButton)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        dismissButton.sizeToFit()
        dismissButton.center = CGPoint(x: 30, y: 40)
    }
    
    @objc func back() {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func onTap() {
        back() // default action is back on tap
    }
}
