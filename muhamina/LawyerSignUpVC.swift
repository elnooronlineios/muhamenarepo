//
//  LawyerSignUpVC.swift
//  muhamina
//
//  Created by Mohamed Nawar on 5/13/20.
//  Copyright © 2020 MuhammedAli. All rights reserved.
//

    import UIKit
   import TextFieldEffects
   import PKHUD
    import Alamofire
   class LawyerSignUpVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
       @IBOutlet var usernameField: HoshiTextField!
       @IBOutlet var emailField: HoshiTextField!
       @IBOutlet var phoneNumberField: HoshiTextField!
       @IBOutlet var passwordField: HoshiTextField!
       @IBOutlet var verifyPasswordField: HoshiTextField!
       @IBOutlet var addPhotoBtn: UIButton!
       @IBOutlet weak var pickCityField: HoshiTextField!
       @IBOutlet weak var chooseLocationInMap: HoshiTextField!
    @IBOutlet weak var addLawyerCard: UIButton!
    var location = SelectedLocation()
       let picker = UIPickerView()
       var cities = CityMain()
       var selectedImageFlag = false
       var selectedCardImageFlag = false
       var choosenCity = -1
       var pickerController = UIImagePickerController()
       var profileOrCardFlag = false
       var imageToken = ""

       @IBAction func dismissView(_ sender: Any) {
           self.dismiss(animated: true, completion: nil)
       }
       var selectedImage = UIImage()
       var selectedCardImage = UIImage()
       @IBAction func addPhotoPressed(_ sender: UIButton) {
        profileOrCardFlag = false
           let alertViewController = UIAlertController(title: "", message: NSLocalizedString("اختر خيارا", comment: ""), preferredStyle: .actionSheet)
           let camera = UIAlertAction(title: NSLocalizedString("الكاميرا", comment: ""), style: .default, handler: { (alert) in
               self.openCamera()
           })
           let gallery = UIAlertAction(title: NSLocalizedString("المعرض", comment: ""), style: .default) { (alert) in
               self.openGallary()
           }
           let cancel = UIAlertAction(title: NSLocalizedString("إلغاء", comment: ""), style: .cancel) { (alert) in
               
           }
           alertViewController.addAction(camera)
           alertViewController.addAction(gallery)
           alertViewController.addAction(cancel)
           alertViewController.popoverPresentationController?.sourceView = self.view
           alertViewController.popoverPresentationController?.permittedArrowDirections = .up
           alertViewController.popoverPresentationController?.sourceRect = sender.frame
           self.present(alertViewController, animated: true, completion: nil)
       }
       @IBOutlet var img: UIImageView!
       
       override func viewDidLoad() {
           super.viewDidLoad()
           img.hero.id = "Ali"
           setupNavBar(vc: self)
           getCities()
           // Do any additional setup after loading the view.
       }
       
       override func didReceiveMemoryWarning() {
           super.didReceiveMemoryWarning()
           // Dispose of any resources that can be recreated.
       }
       
       @IBAction func signUp_btn(_ sender: UIButton) {
           signup()
       }
    
    @IBAction func addCard(_ sender: UIButton) {
        profileOrCardFlag = true
        let alertViewController = UIAlertController(title: "", message: NSLocalizedString("اختر خيارا", comment: ""), preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: NSLocalizedString("الكاميرا", comment: ""), style: .default, handler: { (alert) in
            self.openCamera()
        })
        let gallery = UIAlertAction(title: NSLocalizedString("المعرض", comment: ""), style: .default) { (alert) in
            self.openGallary()
        }
        let cancel = UIAlertAction(title: NSLocalizedString("إلغاء", comment: ""), style: .cancel) { (alert) in
            
        }
        alertViewController.addAction(camera)
        alertViewController.addAction(gallery)
        alertViewController.addAction(cancel)
        alertViewController.popoverPresentationController?.sourceView = self.view
        alertViewController.popoverPresentationController?.permittedArrowDirections = .up
        alertViewController.popoverPresentationController?.sourceRect = sender.frame
        self.present(alertViewController, animated: true, completion: nil)
    }
    
       @IBAction func getLocationPressed(_ sender: Any) {
             let vc = AppStoryboard.Main.viewController(viewControllerClass: AddressPickVC.self)
             vc.delegate = self
             self.present(vc, animated: true, completion: nil)
         }
       func getCities(){
               HUD.show(.progress)
           print(APIs.Instance.getCities())
           callApi(withURL: APIs.Instance.getCities(), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
               HUD.hide()
               switch result {
               case .success(let data):
                   print(data)
                   do {
                       self.cities = try JSONDecoder().decode(CityMain.self, from: data)
                       self.setupPicker()
                   }catch{
                       
                   }
                   
               default:
                   break
               }
           }

       }
       
       func setupPicker(){
           picker.delegate = self
           picker.dataSource = self
           picker.backgroundColor = .white
           picker.showsSelectionIndicator = true
           let toolbar = UIToolbar();
           toolbar.sizeToFit()
           let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker));
           let cancelButton = UIBarButtonItem(title: "إلغاء", style: .plain, target: self, action: #selector(cancelPicker));
           
           toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
           toolbar.barTintColor = #colorLiteral(red: 0.6140939593, green: 0.2171625793, blue: 0.7240038514, alpha: 1)
           toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           pickCityField.inputView = picker
           pickCityField.inputAccessoryView = toolbar
       }
       
       @objc func donePicker(){
              if pickCityField.text == "" {
                  if (cities.data ?? [CityData]()).count > 0 {
                      pickCityField.text = cities.data?[0].name ?? ""
                      choosenCity = cities.data?[0].id ?? 0
                  } else {
                      self.dismiss(animated: true, completion: nil)
                  }
              }
              self.view.endEditing(true)
              
          }
       
          @objc func cancelPicker(){
              pickCityField.text = ""
              self.view.endEditing(true)
          }
       
       func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
             return cities.data?[row].name ?? ""
             
         }
         func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
             pickCityField.text = cities.data?[row].name ?? ""
             choosenCity = cities.data?[row].id ?? -1
         }
         func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
             return (cities.data ?? [CityData]()).count
         }
         func numberOfComponents(in pickerView: UIPickerView) -> Int {
             return 1
         }
         
    func uploadImage() {
    let imgData = UIImageJPEGRepresentation(selectedCardImage, 0.1)!
        
              Alamofire.upload(multipartFormData: { (multipartFormData) in
                
                                    multipartFormData.append(imgData, withName: "files", fileName: "image.jpeg", mimeType: "image/jpeg")

                 
                  print("------\(multipartFormData)")
                  
              }, usingThreshold: UInt64.init(), to: APIs.Instance.uploadImages(), method: .post, headers: APIs.Instance.getHeader()) { (result) in
                  switch result{
                  case .success(let upload, _, _):
                      upload.responseJSON { response in
                          HUD.hide()
                          
                          if let _ = response.error{
                              
                              return
                          }
                          print("Succesfully uploaded----\(response)")
                          //onCompletion?(nil)
                          print(response.result.value)
                          let temp = response.response?.statusCode ?? 400
                          if temp >= 300 {
                              
                              do {
                                  let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                                  makeErrorAlert(subTitle:err.parseError())
                              }catch{
                                  HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                              }
                          }else{
                              do {
                                  let uploadedimage = try JSONDecoder().decode(FileUploaded.self, from: response.data!)
                                  print(uploadedimage.data?.token ?? "")
                                  self.imageToken = uploadedimage.data?.token ?? ""
                              }catch{
                                  HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                              }
                          }
                      }
                  case .failure(let error):
                      print("Error in upload: \(error.localizedDescription)")
                      
                  }
              }
          }
       
        
           
    func signup(){
        
        if selectedImageFlag == false {
            makeDoneAlert(title: "خطأ", SubTitle: "من فضلك اختر صورتك الشخصية", Image: #imageLiteral(resourceName: "icon13"))
            return
        }
        if imageToken == "" {
            makeDoneAlert(title: "خطأ", SubTitle: "من فضلك اختر صورة الكارنية", Image: #imageLiteral(resourceName: "icon13"))
            return
        }
        
        if usernameField.text!.isEmpty == false && emailField.text!.isEmpty == false && phoneNumberField.text!.isEmpty == false && passwordField.text!.isEmpty == false && verifyPasswordField.text!.isEmpty == false  {
            
            let header = [
                              "Accept" : "application/json",
                              "Content-Type":"application/x-www-form-urlencoded"
                          ]
                          let par = ["name":usernameField.text ?? "",
                                      "email": emailField.text ?? "",
                                      "type": "lawyer",
                                     "password": passwordField.text ?? "",
                                     "mobile":phoneNumberField.text ?? "",
                                     "city_id":"\(choosenCity)",
                           "avatar_base64":(UIImageJPEGRepresentation(selectedImage, 0.2)?.base64EncodedString())!,
                                     "location_longitude": location.longitude ?? "", "location_latitude": location.latitude ?? "",
                                     "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid") ?? "",
                                     "tmp_media_tokens": imageToken
                              ] as [String : Any]
                       
                          print(par)
        
        HUD.show(.progress, onView: self.view)
        Alamofire.request(APIs.Instance.registeration(), method: .post, parameters: par, encoding: URLEncoding.default, headers: header).responseString { (response:DataResponse) in
            print(response)
            switch(response.result) {
            case .success(let value):
                HUD.hide()
                let temp = response.response?.statusCode ?? 400
                print(temp)
                if temp >= 300 {
                    print("errorrrr")
                    do {
                        let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                        makeErrorAlert(subTitle:err.parseError())
                    }catch{
                        print("errorrrrelse")
                        
                    }
                }else{
                    
                    do {
                      let user = try JSONDecoder().decode(SingleUserleData.self, from: response.data!)
                        if user.data?.activated ?? "<null>" == "<null>" {
                            makeDoneAlert(title: "تنبية", SubTitle: "سوف يتم تفعيل حسابك في أقرب وقت وأرسال تنبية بذلك", Image: #imageLiteral(resourceName: "icon13"))
                        }
                           UIApplication.shared.registerForRemoteNotifications()
                      UserDefaults.standard.set(response.data!, forKey:"user")
                            UserDefaults.standard.set(false, forKey: "presentedCountryPicker")
                           User.shared.fetchUser()
                           NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier1"), object: nil)
                           self.dismissWithTransition()
                    }catch{
                        HUD.flash(.label(NSLocalizedString("Error Try Again", comment: "حدث خطأ برجاء اعادة المحاولة")), delay: 1.0)
                    }
                }
            case .failure(_):
                HUD.hide()
                let lockString = NSLocalizedString("Something went wrong please try again later", comment: "حدث خطأ برجاء اعادة المحاولة")
                HUD.flash(.label(lockString), delay: 1.0)
                break
            }
        }
      }
    }
    func convertImageToBase64String (img: UIImage) -> String {
        let imageData:NSData = UIImageJPEGRepresentation(img, 0.50)! as NSData //UIImagePNGRepresentation(img)
        let imgString = imageData.base64EncodedString(options: .init(rawValue: 0))
        return imgString
    }
       
       func dismissWithTransition()  {
         
               self.dismiss(animated: false, completion: {
                   NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didLogin"), object: nil, userInfo: nil)
               })
               
           
       }
       
       
       
       func openCamera() {
           if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
               pickerController.delegate = self
               self.pickerController.sourceType = UIImagePickerController.SourceType.camera
               pickerController.allowsEditing = true
               self .present(self.pickerController, animated: true, completion: nil)
           }
           else {
               
               let alertViewController = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
               let cancel = UIAlertAction(title: "Ok", style: .default) { (alert) in
                   
               }
               alertViewController.addAction(cancel)
               self.present(alertViewController, animated: true, completion: nil)
           }
       }
       func openGallary() {
           if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary) {
               
               pickerController.delegate = self
               pickerController.sourceType = UIImagePickerController.SourceType.photoLibrary
               pickerController.allowsEditing = true
               pickerController.navigationBar.isTranslucent = false
               pickerController.navigationBar.barTintColor = #colorLiteral(red: 0.4459182024, green: 0.1324112117, blue: 0.8598670363, alpha: 1) // Background color
               pickerController.navigationBar.tintColor = .white
               self.present(pickerController, animated: true, completion: nil)
           }
       }
       
       override func viewWillAppear(_ animated: Bool) {
           if !isModal{
               self.navigationItem.leftBarButtonItem = nil
           }
       }
       public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
           if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if profileOrCardFlag {
                selectedCardImageFlag = true
                selectedCardImage = pickedImage
                addLawyerCard.setImage(pickedImage, for: .normal)
                uploadImage()
            }else{
              addPhotoBtn.setImage(pickedImage, for: .normal)
              selectedImage = pickedImage
              selectedImageFlag = true
           }
        }
           dismiss(animated:true, completion: nil)
       }
       public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
           self.dismiss(animated: true, completion: nil)
           print("Cancel")
       }

       
   }

//MARK:- ShareLocationProtocol
extension LawyerSignUpVC: ShareLocationProtocol {
    func share(_ location: SelectedLocation) {
    self.location = location
    chooseLocationInMap.text = location.address
    }
}

