//
//  Errors.swift
//  muhamina
//
//  Created by mouhammed ali on 10/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation
import Foundation

class ErrorHandler : Decodable{
    var message : String?
    var errors : ErrorTypes!
    
    func parseError() -> String {
        var str = ""
        if let temp = self.errors?.email{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.name{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.password{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.image_base64{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.expected_date{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.notify_at{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.mobile{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        
        
        if let temp = self.errors?.facebookError{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.twitterError{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.snapchatError{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.instagramError{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.websiteError{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        
        if let temp = self.errors?.location_latitude{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        if let temp = self.errors?.location_longitude{
            if temp.count != 0{
                str.append(temp[0])
                
            }
        }
        
        if str == "" {
            if message == "No query results for model [App\\Models\\City]." {
                return "لا يوجد محامين في هذه المدينة"
            }
            return message ?? ""
        }
        
        return str
    }
    
    
}
struct ErrorImd : Decodable {
    var errors : [ErrorTypes]?
}
struct ErrorTypes : Decodable {
    var email : [String]?
    var password : [String]?
    var name : [String]?
    var notify_at : [String]?
    var expected_date : [String]?
    var image_base64 : [String]?
    var mobile:[String]?
    var location_latitude:[String]?
    var location_longitude:[String]?
    
    
    var facebookError : [String]?
    var twitterError : [String]?
    var instagramError : [String]?
    var snapchatError : [String]?
    var websiteError:[String]?
    
    
    //    var social_media.facebook : [String]?
    enum CodingKeys:String, CodingKey {
        case facebookError = "social_media.facebook"
        case twitterError = "social_media.twitter"
        case instagramError = "social_media.instgram"
        case snapchatError = "social_media.snapchat"
        case websiteError = "social_media.website"
        case email
        case password
        case name
        case notify_at
        case expected_date
        case image_base64
        case mobile
        case location_latitude
        case location_longitude
    }
    //    var expected_date : [String]?
    //    var image_base64 : [String]?
    //    var mobile:[String]?
    
    
    
    
}

