//
//  TafasilElTalabViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class TafasilElTalabViewController: UIViewController {
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    var selectedDate = ""
    var selectedTime = ""
    
    @IBOutlet weak var DateTextField: DesignableTextField!
    @IBOutlet weak var TimeTextField: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.backgroundColor = .white
        timePicker.backgroundColor = .white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        showDatePicker()
        showTimePicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }

    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        let loc = Locale(identifier: "ar")
        self.datePicker.locale = loc
        self.datePicker.minimumDate = Date()
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "إلغاء" , style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        DateTextField.inputAccessoryView = toolbar
        DateTextField.inputView = datePicker
        
    }
    func showTimePicker(){
        //Formate Date
        timePicker.datePickerMode = .time
        let loc = Locale(identifier: "ar")
        self.timePicker.locale = loc
        self.timePicker.minimumDate = Date()
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(doneTimePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "إلغاء", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        TimeTextField.inputAccessoryView = toolbar
        TimeTextField.inputView = timePicker
        
    }

    @objc func doneTimePicker(){
        let formatter = DateFormatter()
        formatter.timeStyle = .short
        formatter.locale = NSLocale(localeIdentifier: "ar") as Locale!
        TimeTextField.text = formatter.string(from: timePicker.date)
        self.view.endEditing(true)
        formatter.dateFormat = "HH:mm"
        formatter.locale = Locale(identifier: "en")
        selectedTime = formatter.string(from: timePicker.date)
        
    }
    @objc func donedatePicker(){
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy"
            formatter.locale = NSLocale(localeIdentifier: "ar") as Locale!
            DateTextField.text = formatter.string(from: datePicker.date)
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = Locale(identifier: "en")
        selectedDate = formatter.string(from: datePicker.date)
        
            self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }

    @IBAction func backBtn(_ sender: Any) {
        dismissing = 1
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendPressed(_ sender: Any) {
        if DateTextField.text == "" || TimeTextField.text == "" {
            makeErrorAlert(subTitle: "من فضلك قم بإختيار الوقت و التاريخ")
            return
        }
        dismissing = 1
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "acceptOrder"), object: nil, userInfo: ["date":"\(self.selectedDate) \(self.selectedTime)"])
        }
    }
    
}
