//
//  Order.swift
//  muhamina
//
//  Created by mouhammed ali on 11/2/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation
class Orders:Decodable {
    var data:Order_data?
}
class SingleOrder:Decodable {
    var data:Order?
}
class Order:Decodable {
    var client:Client?
    var date:date?
    var details:String?
    var id:Int?
    var lawyer:Lawyer?
    var record:String?
    var status:String?
    var title:String?
    var created_at:date?
    var issue_type:Issue?
}

class date:Decodable {
    var date:String?
}
class IssueType:Decodable{
    var id:Int?
    var name:String?
}

class Order_data : Decodable {
    
    var order_obj : Order?
    var order_arr : [Order]?
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        if let x = try? container.decode(Order.self){
            self.order_obj = x
            return
        }else if let x = try? container.decode([Order].self){
            self.order_arr = x
            return
        }
        
        throw DecodingError.typeMismatch(Orders.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "wrong"))
    }
    
    
}
