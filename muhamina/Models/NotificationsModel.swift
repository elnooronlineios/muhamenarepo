//
//  NotificationsModel.swift
//  muhamina
//
//  Created by ElNoorOnline on 12/3/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation

class NotificationsModel:Decodable {
    var data:[DataModel]?
}
class DataModel: Decodable {
    var image:String?
    var title:String?
    var body:String?
    var date:String?
    var data:appoint_id?
   
}

class appoint_id:Decodable {
    var appointment_id:Int?
}
class FileUploaded:Decodable {
    var data:FileUploadedData?
}
class FileUploadedData: Decodable {
    var token:String?
}
