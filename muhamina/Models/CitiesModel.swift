//
//  CitiesModel.swift
//  muhamina
//
//  Created by mouhammed ali on 10/27/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation
class CityMain:Decodable {
    var data:[CityData]?
    func getNames()->[String] {
        var temp = [String]()
        for item in (data ?? [CityData]())  {
            temp.append(item.name ?? "")
        }
        return temp
    }
}
class CityData: Decodable {
    var id:Int?
    var name:String?
}
