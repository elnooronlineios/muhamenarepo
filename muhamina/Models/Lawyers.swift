//
//  Lawyers.swift
//  muhamina
//
//  Created by mouhammed ali on 10/28/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation
class Lawyers:Decodable {
    var data:[Lawyer]?
    var meta: MetaData?
}
class Lawyer: Decodable {
    var avatar:String?
    var name:String?
    var city:CityData?
    var email:String?
    var id:Int?
    var latitude:String?
    var certifications:[Certification]?
    var longitude:String?
    var mobile:String?
    var type:String?
    var issues:[Issue]?
}
class Certification: Decodable {
    var id:Int?
    var name:String?
    
}
class MetaData: Decodable {
    var last_page:Int?    
}
class Issue: Decodable {
    var id:Int?
    var name:String?
    
}
