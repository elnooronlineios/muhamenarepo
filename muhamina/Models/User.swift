//
//  User.swift
//  muhamina
//
//  Created by mouhammed ali on 10/22/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation
import UIKit
struct SingleUserleData:Decodable {
     var data : UserData?
}
struct User : Decodable {
    var data : UserData?
    var token = ""
    static var shared = User()
    public mutating func logout() {
        UIApplication.shared.unregisterForRemoteNotifications()
        UserDefaults.standard.removeObject(forKey: "updatedAvatar")
        UserDefaults.standard.removeObject(forKey: "user")
        UserDefaults.standard.set(false, forKey: "presentedCountryPicker")
        self.token = ""
        
    }
    init() {
        fetchUser()
    }
    public func isLoggedIn() -> Bool {
        if self.token == "" {
            return false
        }
        return true
    }
    mutating func fetchUser(){
        if let data = UserDefaults.standard.object(forKey: "user") as? Data {
            do{
                self = try JSONDecoder().decode(User.self, from: data)
                print(self.token)
            }catch{
                self.token = ""
                print(error)}
        }
        if let img = UserDefaults.standard.object(forKey: "updatedAvatar") as? String {
            self.data?.avatar = img
        }
        
    }
}

class UserData: Decodable {
    var city:City?
    var email:String?
    var id:Int?
    var mobile:String?
    var type:String?
    var avatar:String?
    var name:String?
    var activated:String?
    
    
    class City: Decodable {
        var country:Country?
        var name:String?

        class Country: Decodable {
             var name:String?
        }
    }
    
    
}



