//
//  Client.swift
//  muhamina
//
//  Created by mouhammed ali on 11/2/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation
class Client: Decodable {
    var email:String?
    var id:Int?
    var image:String?
    var name:String?
    var mobile:String?
}
