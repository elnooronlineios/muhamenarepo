//
//  Cases.swift
//  muhamina
//
//  Created by mouhammed ali on 11/3/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import Foundation
class CaseTypes:Decodable {
    var data:[CaseType]?
    func getCaseTypeNames()->[String] {
        var temp = [String]()
        for item in data ?? [CaseType]() {
            temp.append(item.name ?? "")
        }
        return temp
    }
}
class CaseType:Decodable {
    var id:Int?
    var name:String?
}
