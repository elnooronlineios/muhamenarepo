//
//  Regulations.swift
//  muhamina
//
//  Created by Mohamed on 6/11/20.
//  Copyright © 2020 MohamedHassanNawar. All rights reserved.
//

import Foundation

struct Regulations: Decodable {
    var data: [RegulationsDetails]?
}

struct RegulationsData: Decodable {
    var data: [RegulationsDetails]?
}

struct RegulationsDetails: Decodable {
    var id: Int?
    var name: String?
    var description: String?
    var parent_id: ParentID?
}

struct ParentID: Decodable {
    var id: Int?
    var name: String?
}
