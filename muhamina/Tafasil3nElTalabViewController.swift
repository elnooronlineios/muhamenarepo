//
//  Tafasil3nElTalabViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/28/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import PKHUD
class Tafasil3nElTalabViewController: UIViewController {
    var order = Order()
    var observer: NSKeyValueObservation?
    @IBOutlet var imgView: UIImageView!
    var player: AVPlayer!
    
    @IBOutlet var bottomStackHeight: NSLayoutConstraint!
    @IBOutlet var caseTypeLbl: UILabel!
    @IBOutlet var orderLbl: UILabel!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var durationSlider: UISlider!
    
    @IBOutlet weak var audio_view: UIView!
    var is_play:Bool = false
    
    @IBAction func reject(_ sender: Any) {
        HUD.show(.progress)
        callApi(withURL: APIs.Instance.updateAppointment(id:order.id ?? 0), method: .put, headers: APIs.Instance.getHeader(), Parameter: ["status":"rejected"]) { (result) in
            
            HUD.hide()
            
            switch result {
            case .success(let data):

                
                if let vcs = self.navigationController?.viewControllers {
                    if vcs.count >= 2 {
                        if let prevVC = vcs[vcs.count - 2] as? AltalabatAlwaredaViewController {
                            prevVC.getMyOrders()
                            self.navigationController?.popViewController(animated: true)
                        }else{
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
                
                
            default:
                break
            }
        }
    }
    
    
    
    @IBOutlet var playBtn: UIButton!
    @IBAction func playPressed(_ sender: UIButton) {
    let url  = URL.init(string: order.record!)
        play(url: url!)
    }
    
    func play(url:URL) {
        print("playing \(url)")

        do {

            let playerItem = AVPlayerItem(url: url)

            self.player = try AVPlayer(playerItem:playerItem)
            player!.volume = 1.0
            player!.play()
        } catch let error as NSError {
            self.player = nil
            print(error.localizedDescription)
        } catch {
            print("AVAudioPlayer init failed")
        }
    }
    @IBAction func sliderChanged(_ sender: Any) {
    }
    @IBOutlet var descLbl: UILabel!
    @IBAction func callBtn(_ sender: Any) {
        guard let number = URL(string: "tel://" + (order.client?.mobile ?? "")) else { return }
        UIApplication.shared.open(number)
    }
    var heroId = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLbl.text = order.client?.name ?? ""
        orderLbl.text = order.title ?? ""
        caseTypeLbl.text = order.issue_type?.name ?? ""
        descLbl.text = order.details ?? ""
        imgView.sd_setImage(with: URL(string: order.client?.image ?? ""), completed: nil)
        //durationSlider.setThumbImage(UIImage(named: "circular-shape-silhouette"), for: .normal)
        durationSlider.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
        
        if order.record != nil{
            setupPlayer()
            print("\(order.record)")
        }else {
            audio_view.isHidden = true
        }
        if (order.status ?? "") != "pending" {
            bottomStackHeight.constant = 0
            self.view.layoutSubviews()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(acceptOrder), name: NSNotification.Name(rawValue: "acceptOrder"), object: nil)
        //viewWithTag(33)?.hero.id = heroId+"-3"
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
        //  let t = CMTime(seconds: 1, preferredTimescale: )
        
        
    }
    func setupPlayer(){
        //    let url  = URL.init(string:"https://raw.githubusercontent.com/robovm/apple-ios-samples/master/avTouch/sample.m4a")
        
        let url  = URL.init(string: order.record!)
        print()
        // let playerItem: AVPlayerItem = AVPlayerItem(url: url!)
        
        
        
        
        
        // Create asset to be played
        let asset = AVAsset(url: url!)
        print(asset)
        let assetKeys = [
            "playable",
            "hasProtectedContent"
        ]
        // Create a new AVPlayerItem with the asset and an
        // array of asset keys to be automatically loaded
        let playerItem = AVPlayerItem(asset: asset,
                                      automaticallyLoadedAssetKeys: assetKeys)
        print(playerItem)
        // Register as an observer of the player item's status property
        self.observer = playerItem.observe(\.status, options:  [.new, .old], changeHandler: { (playerItem, change) in
            if playerItem.status == .readyToPlay{
                
                //       print("+++++ \(playerItem.status) +++++\(self.player.currentItem)")
                
                self.durationSlider.maximumValue = Float(CMTimeGetSeconds(self.player.currentItem?.asset.duration ?? CMTime()))
                Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.updateSlider), userInfo: nil, repeats: true)
                
            }
            
        })
        
        // Associate the player item with the player
        player = AVPlayer(playerItem: playerItem)
        
        
    }
    func rejectService(){
        func getCities(){
            HUD.show(.progress)
            print(APIs.Instance.getCities())
            callApi(withURL: APIs.Instance.addAppointment(id:order.id ?? 0), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
                HUD.hide()
                switch result {
                case .success(let data):
                    print(data)
                    do {
                        //                        self.cities = try JSONDecoder().decode(CityMain.self, from: data)
                        //                        self.setupPicker()
                    }catch{
                        
                    }
                    
                default:
                    break
                }
            }
            
        }
        
    }
    @objc func updateSlider(){
        
        if player != nil {
            durationSlider.value = Float(CMTimeGetSeconds(player.currentTime()))
            
            let nn =  Float(CMTimeGetSeconds(player.currentTime()))
            
            
            if durationSlider.maximumValue == nn{
                
                print("reset-----")
                
                is_play = false
                playBtn.setImage(UIImage(named: "play-button"), for: .normal)
                self.durationSlider.transform = CGAffineTransform(scaleX: 0.85, y: 0.85)
                player = nil
                self.setupPlayer()
                
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        if player != nil {
            
            player.pause()
            //    player.removeTimeObserver(observer)
            player = nil
            
        }
        super.viewWillDisappear(animated)
        
    }
    
    @IBAction func accept(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: TafasilElTalabViewController.self)
        let navVC = UINavigationController.init(rootViewController: vc)
        self.present(navVC, animated: true, completion: nil)
        
    }
    @objc func acceptOrder(_ notification: NSNotification) {
        if let dict = notification.userInfo {
            if let date = dict["date"] as? String {
                HUD.show(.progress)
                let par = ["status":"accepted",
                           "date":date]
                print(par)
                print(APIs.Instance.updateAppointment(id:order.id ?? 0))
                print(APIs.Instance.getHeader())
                callApi(withURL: APIs.Instance.updateAppointment(id:order.id ?? 0), method: .put, headers: APIs.Instance.getHeader(), Parameter: par) { (result) in
                    
                    HUD.hide()
                    
                    switch result {
                    case .success(let data):
                        
                        
                        if let vcs = self.navigationController?.viewControllers {
                            if vcs.count >= 2 {
                                if let prevVC = vcs[vcs.count - 2] as? AltalabatAlwaredaViewController {
                                    prevVC.getMyOrders()
                                    self.navigationController?.popViewController(animated: true)
                                }else{
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                        
                        
                    default:
                        break
                    }
                }
            }
            
        }
        
    }
}
