//
//  UITextField.swift
//  muhamina
//
//  Created by MACBOOK on 7/25/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

@IBDesignable
class  DesignableTextField: UITextField  {
    
    @IBInspectable var placeHolderColor:UIColor?{
        didSet{
            attributedPlaceholder = NSAttributedString(string: placeholder!,
            attributes: [NSAttributedStringKey.foregroundColor: placeHolderColor])
        }
    }
    @IBInspectable var rightImage:UIImage?{
        didSet{
            updateView()
        }
    }
    @IBInspectable var rightPadding:CGFloat = 0 {
        didSet{
            updateView()
        }
    }
  

    func updateView() {
        if let image = rightImage {
            rightViewMode = .always
            let imageView = UIImageView(frame: CGRect(x: rightPadding - 10 , y: 0, width: 20, height: 20))
            imageView.image = image
            var width = rightPadding + 20
            if borderStyle == UITextBorderStyle.none || borderStyle == UITextBorderStyle.line {
               width =  width + 5
            }
            let view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
             self.rightView = view
            self.leftViewMode = UITextFieldViewMode.always
            self.placeholder = placeholder
        }else{
            rightViewMode = .never
        }
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ? placeholder!: "", attributes:  [kCTForegroundColorAttributeName as NSAttributedStringKey : tintColor])
    }
    
}
