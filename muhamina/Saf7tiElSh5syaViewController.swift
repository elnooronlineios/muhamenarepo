//
//  Saf7tiElSh5syaViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/28/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit

class Saf7tiElSh5syaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
}
