//
//  EditProfileVC.swift
//  muhamina
//
//  Created by mouhammed ali on 11/4/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import PKHUD
import TextFieldEffects
public var dismissing = 0
class EditProfileVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet var addPhotoBtn: UIButton!
    var selectedImageFlag = false
    @IBOutlet var oldPassField: HoshiTextField!
    @IBOutlet var newPassField: HoshiTextField!
    
    @IBAction func editinfo(_ sender: Any) {
        editProfile()
    }
    var selectedImage = UIImage()
     var pickerController = UIImagePickerController()
    @IBAction func addPhotoPressed(_ sender: UIButton) {
        let alertViewController = UIAlertController(title: "", message: NSLocalizedString("اختر خيارا", comment: ""), preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: NSLocalizedString("الكاميرا", comment: ""), style: .default, handler: { (alert) in
            dismissing = 1
            self.openCamera()
        })
        let gallery = UIAlertAction(title: NSLocalizedString("المعرض", comment: ""), style: .default) { (alert) in
            dismissing = 1
            self.openGallary()
        }
        let cancel = UIAlertAction(title: NSLocalizedString("إلغاء", comment: ""), style: .cancel) { (alert) in
            
        }
        alertViewController.addAction(camera)
        alertViewController.addAction(gallery)
        alertViewController.addAction(cancel)
        alertViewController.popoverPresentationController?.sourceView = self.view
        alertViewController.popoverPresentationController?.permittedArrowDirections = .up
        alertViewController.popoverPresentationController?.sourceRect = sender.frame
        self.present(alertViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addPhotoBtn.sd_setImage(with: URL(string: User.shared.data?.avatar ?? ""), for: .normal, completed: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            pickerController.delegate = self
            self.pickerController.sourceType = UIImagePickerControllerSourceType.camera
            pickerController.allowsEditing = true
            self .present(self.pickerController, animated: true, completion: nil)
        }
        else {
            
            let alertViewController = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Ok", style: .default) { (alert) in
                
            }
            alertViewController.addAction(cancel)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = true
            pickerController.navigationBar.isTranslucent = false
            pickerController.navigationBar.barTintColor = #colorLiteral(red: 0.4459182024, green: 0.1324112117, blue: 0.8598670363, alpha: 1) // Background color
            pickerController.navigationBar.tintColor = .white
            self.present(pickerController, animated: true, completion: nil)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        selectedImageFlag = true
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        addPhotoBtn.setImage(image, for: .normal)
        selectedImage = image
        self.dismiss(animated: true) {
            dismissing = 0
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true) {
            dismissing = 0
        }
        print("Cancel")
    }
    func editProfile(){
        if newPassField.text == "" && oldPassField.text == "" && selectedImageFlag == false {
            self.navigationController?.popViewController(animated: true)
            return
        }
        var par = [String:AnyObject]()
        if newPassField.text != "" || oldPassField.text != ""{
            par["password"] = (newPassField.text ?? "") as AnyObject
            par["old_password"] = (oldPassField.text ?? "") as AnyObject
        }
        if selectedImageFlag {
            par["avatar"] = UIImageJPEGRepresentation(selectedImage, 0.7)?.base64EncodedString() as AnyObject
        }

            print(par)

            HUD.show(.progress)
        var url = APIs.Instance.updateLawyerProfile()
        if User.shared.data?.type == "user" {
            url = APIs.Instance.updateClientProfile()
        }
            callApi(withURL: url, method: .put, headers: APIs.Instance.getHeader(), Parameter: par) { (result) in
                HUD.hide()
                switch result {
                case .success(let data):
                    do {
                        let user = try JSONDecoder().decode(SingleUserleData.self, from: data)
                        UserDefaults.standard.set(user.data?.avatar ?? "", forKey: "updatedAvatar")
                        User.shared.fetchUser()
                        
                    }catch{
                        
                    }
                    if let parentVC = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 2] {
                        if let parentVC = parentVC as? ProfileVC {
                            parentVC.userImage.sd_setImage(with: URL(string:User.shared.data?.avatar ?? ""), completed: nil)
                        }}
                    self.navigationController?.popViewController(animated: true)
                default:
                    break
                }
            }
        }
}
