//
//  HagzMaw3edViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/27/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import iOSDropDown
import Font_Awesome_Swift
import AVFoundation
import Alamofire
import PKHUD
class HagzMaw3edViewController: UIViewController , UIPickerViewDelegate , UIPickerViewDataSource,AVAudioRecorderDelegate, AVAudioPlayerDelegate  {
    var caseTypes = CaseTypes()
    var audioPlayer : AVAudioPlayer!
    var recordingSession:AVAudioSession!
    var audioRecorder:AVAudioRecorder!
    var settings = [String : Int]()
    var lawyerId = 0
    var totalTime = 0
    var selectedCaseType = -1
    var didRecord = false
     var countdownTimer: Timer!
    let picker = UIPickerView()

    @IBAction func confirmBtn(_ sender: Any) {
        if addressField.text != "" && selectedCaseType != -1 && detailsField.text != "" {
            submitRequest()
            return
        }
        makeErrorAlert(subTitle: "من فضلك قم بملأ جميع الحقول")
    }
    @IBAction func playPressed(_ sender: Any) {
        if !audioRecorder.isRecording {
            self.audioPlayer = try! AVAudioPlayer(contentsOf: audioRecorder.url)
            self.audioPlayer.prepareToPlay()
            self.audioPlayer.delegate = self
            audioPlayer.volume = 1
            do {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
            } catch _ {
            }
            self.audioPlayer.play()
        }
    }
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var recordingLbl: UILabel!
    @IBOutlet var durationLbl: UILabel!
    @IBAction func startRecording(_ sender: Any) {
         UIImpactFeedbackGenerator().impactOccurred()
        self.startRecording()
        recordingLbl.isHidden = false
        playBtn.isHidden = true
        startTimer()
    }
    @IBAction func stopRecording(_ sender: Any) {
       self.finishRecording(success: true)
        UINotificationFeedbackGenerator().notificationOccurred(.success)
        recordingLbl.isHidden = true
        playBtn.isHidden = false
        endTimer()
        didRecord = true
       
    }
    
    @IBOutlet var detailsField: UITextView!
    @IBOutlet var addressField: DesignableTextField!
    @IBOutlet weak var caseType: DesignableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        getCaseCategories()
        askForRecordingPremission()
        picker.delegate = self
        picker.dataSource = self
        picker.backgroundColor = .white
        picker.showsSelectionIndicator = true
        showDatePicker()
        caseType.setLeftViewFAIcon(icon: .FAAngleDown, leftViewMode: .always, textColor: .gray, backgroundColor: .clear, size: nil)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes

    }
    func showDatePicker(){
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "تم", style: .plain, target: self, action: #selector(donePicker));

        let cancelButton = UIBarButtonItem(title: "إلغاء", style: .plain, target: self, action: #selector(cancelPicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        toolbar.barTintColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        toolbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        caseType.inputView = picker
        caseType.inputAccessoryView = toolbar
        
    }
    @objc func donePicker(){
        if caseType.text == "" {
            caseType.text = (caseTypes.data ?? [CaseType]())[0].name ?? ""
            selectedCaseType = (caseTypes.data ?? [CaseType]())[0].id ?? -1
        }
        self.view.endEditing(true)
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return (caseTypes.data ?? [CaseType]()).count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return (caseTypes.data ?? [CaseType]())[row].name ?? ""
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        caseType.text = (caseTypes.data ?? [CaseType]())[row].name ?? ""
        selectedCaseType = (caseTypes.data ?? [CaseType]())[row].id ?? -1
        
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func askForRecordingPremission(){
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        print(soundURL)
        return soundURL as NSURL?
    }
    func startRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        if success {
            print(success)
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print(flag)
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?){
        print(error.debugDescription)
    }
    internal func audioPlayerBeginInterruption(_ player: AVAudioPlayer){
        print(player.debugDescription)
    }
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        //     let hours: Int = totalSeconds / 3600
        return String(format: "%02d:%02d", minutes, seconds)
    }
    func endTimer() {
        countdownTimer.invalidate()
        totalTime = 0
    }
    @objc func updateTime() {
        totalTime += 1
        durationLbl.text = "\(timeFormatted(totalTime))"
        
    }
    func startTimer() {
        countdownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
    }
    func submitRequest(){
        print(APIs.Instance.addAppointment(id:lawyerId))
        HUD.show(.progress)
        var par = ["title":addressField.text ?? "",
                   "issue_type_id":selectedCaseType,
                   "details":detailsField.text ?? ""] as [String : Any]
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in par {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if self.didRecord {
                multipartFormData.append(self.audioRecorder.url, withName: "record", fileName: "sound.m4a", mimeType: "audio/m4a")
              
                print("------record ----")
                
//                let audioData =  try? Data(contentsOf: (self.audioRecorder?.url)!)
//                 let encodedString = audioData?.base64EncodedString()
//                
//                multipartFormData.append(audioData as! Data, withName: "audio", fileName: "sound.m4a", mimeType: "audio/m4a")
//               
            }
            
            print("------\(multipartFormData)")
            
        }, usingThreshold: UInt64.init(), to: APIs.Instance.addAppointment(id:lawyerId), method: .post, headers: APIs.Instance.getHeader()) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseJSON { response in
                    HUD.hide()
                    
                    if let _ = response.error{
                        
                        return
                    }
                    print("Succesfully uploaded----\(response)")
                    //onCompletion?(nil)
                    print(response.result.value)
                    let temp = response.response?.statusCode ?? 400
                    if temp >= 300 {
                        
                        do {
                            let err = try JSONDecoder().decode(ErrorHandler.self, from: response.data!)
                            makeErrorAlert(subTitle:err.parseError())
                        }catch{
                            HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        }
                    }else{
                        do {
                            let order = try JSONDecoder().decode(SingleOrder.self, from: response.data!)
                            self.refreshView()
                            let vc = AppStoryboard.Main.viewController(viewControllerClass: TmEl2rsalViewController.self)
                            vc.orderNumber = order.data?.id ?? 0
                            vc.modalPresentationStyle = .overCurrentContext
                            vc.modalTransitionStyle = .crossDissolve
                            self.present(vc, animated: true, completion: nil)
                            
                        }catch{
                            HUD.flash(.labeledError(title: nil, subtitle: "حدث خطا بالرجاء التأكد من صحة البيانات المدخلة"), delay: 1.0)
                        }
                    }
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                
            }
        }
    }
    func refreshView() {
        detailsField.text = ""
        addressField.text = ""
        caseType.text = ""
        selectedCaseType = -1
        durationLbl.text = "إرسال رسالة صوتية"
        durationLbl.isHidden = false
        recordingLbl.isHidden = true
        playBtn.isHidden = true
    }
    func getCaseCategories(){
        HUD.show(.progress)
        
        callApi(withURL: APIs.Instance.getCaseTypes(), method: .get, headers: APIs.Instance.getHeader(), Parameter: [:]) { (result) in
            HUD.hide()
            switch result {
            case .success(let data):
                print(data)
                do {
                    self.caseTypes = try JSONDecoder().decode(CaseTypes.self, from: data)
                    self.picker.reloadAllComponents()
                }catch{
                    
                }
                
            default:
                break
            }
        }
    }
    @objc func cancelPicker(){
        caseType.text = ""
        selectedCaseType = -1
        picker.selectRow(0, inComponent: 0, animated: true)
        self.view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }
}
