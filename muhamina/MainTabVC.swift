//
//  MainTabVC.swift
//  muhamina
//
//  Created by mouhammed ali on 10/22/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
var presentedBefore = false
class MainTabVC: UITabBarController,UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        print(UserDefaults.standard.string(forKey: "onesignalid") ?? "")
        hero.isEnabled = true
        setupTabBar()
        self.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification1(notification:)), name: Notification.Name("NotificationIdentifier1"), object: nil)
    }
    @objc func methodOfReceivedNotification1(notification: Notification) {
        setupTabBar()
    }
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController.tabBarItem.image == UIImage(named: "icon06") && !User.shared.isLoggedIn() {
            
            let vc = AppStoryboard.Main.viewController(viewControllerClass: laweyrOrUserVC.self)
            let navVc = UINavigationController(rootViewController: vc)
//            vc.hideButton = false
            self.present(navVc, animated: true, completion: nil)
            return false
        }
        return true
    }
    override func viewDidAppear(_ animated: Bool) {
        print(UserDefaults.standard.bool(forKey: "presentedCountryPicker"))
        if User.shared.data?.type == "user" {
        if !UserDefaults.standard.bool(forKey: "presentedCountryPicker") {
            UserDefaults.standard.set(true, forKey: "presentedCountryPicker")
            let vc = AppStoryboard.Main.viewController(viewControllerClass: AkhterAlmadinaViewController.self)
            let navVc = UINavigationController(rootViewController: vc)
            self.present(navVc, animated: false, completion: nil)
            
            }
            
        }
        if !presentedBefore && !User.shared.isLoggedIn() {
            presentedBefore = true
            let vc = AppStoryboard.Main.viewController(viewControllerClass: laweyrOrUserVC.self)
            let navVc = UINavigationController(rootViewController: vc)
            self.present(navVc, animated: false, completion: nil)
        }
        if dismissing == 1 {
            dismissing = 0
        }else{
            setupTabBar()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupTabBar(){
        self.tabBar.tintColor = #colorLiteral(red: 0.2862745098, green: 0.368627451, blue: 0.5607843137, alpha: 1)
        
        let regulationsVC = AppStoryboard.Menus.viewController(viewControllerClass: RegulationsShowVC.self)
        let navRegulations = UINavigationController.init(rootViewController: regulationsVC)
        navRegulations.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "law"), selectedImage: UIImage(named: "law"))
        
        
        if !User.shared.isLoggedIn() {
            let homeVC = AppStoryboard.Main.viewController(viewControllerClass: MainViewController.self)
            let navHome = UINavigationController.init(rootViewController: homeVC)
            navHome.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon04"), selectedImage: UIImage(named: "icon04"))
            
            homeVC.hero.isEnabled = true
            navHome.hero.isEnabled = true
            let userVC = AppStoryboard.Main.viewController(viewControllerClass: laweyrOrUserVC.self)
            userVC.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon06"), selectedImage: UIImage(named: "icon06"))
            let navUser = UINavigationController.init(rootViewController: userVC)
            
            self.viewControllers = [navUser,navRegulations,navHome]
            self.selectedIndex = 2
            
        }
        else if User.shared.data?.type == "user" {
            

            let homeVC = AppStoryboard.Main.viewController(viewControllerClass: MainViewController.self)
            let navHome = UINavigationController.init(rootViewController: homeVC)
            navHome.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon04"), selectedImage: UIImage(named: "icon04"))
            homeVC.hero.isEnabled = true
            navHome.hero.isEnabled = true
            
            
            
            
            let userVC = AppStoryboard.Main.viewController(viewControllerClass: ProfileVC.self)
            let navUser = UINavigationController.init(rootViewController: userVC)
            navUser.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon06"), selectedImage: UIImage(named: "icon06"))
            
            
            
            
            
            let ordersVC = AppStoryboard.Main.viewController(viewControllerClass: TalabatiViewController.self)
            let navOrders = UINavigationController.init(rootViewController: ordersVC)
            navOrders.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon17"), selectedImage: UIImage(named: "icon17"))
            
            
            
            
           // let contactVC = AppStoryboard.Main.viewController(viewControllerClass: TafasilElTalabViewController.self)
            let contactVC = AppStoryboard.Main.viewController(viewControllerClass: ContactUs.self)
            let navContact = UINavigationController.init(rootViewController: contactVC)
            navContact.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon05"), selectedImage: UIImage(named: "icon05"))
            
            
            self.viewControllers = [navContact,navUser,navOrders,navRegulations,navHome]
            self.selectedIndex = 4
            
        }else if User.shared.data?.type == "lawyer"  {
            let homeVC = AppStoryboard.Main.viewController(viewControllerClass: AltalabatAlwaredaViewController.self)
            let navHome = UINavigationController.init(rootViewController: homeVC)
            navHome.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon04"), selectedImage: UIImage(named: "icon04"))
            homeVC.hero.isEnabled = true
            navHome.hero.isEnabled = true
            
            
            
            
            let userVC = AppStoryboard.Main.viewController(viewControllerClass: ProfileVC.self)
            let navUser = UINavigationController.init(rootViewController: userVC)
            navUser.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon06"), selectedImage: UIImage(named: "icon06"))

            
            let contactVC = AppStoryboard.Main.viewController(viewControllerClass: ContactUs.self)
            let navContact = UINavigationController.init(rootViewController: contactVC)
            navContact.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "icon05"), selectedImage: UIImage(named: "icon05"))
            
            
            self.viewControllers = [navContact,navUser,navRegulations,navHome]
            self.selectedIndex = 3
        }
        
    }
    override func viewWillLayoutSubviews() {
        var tabFrame = self.tabBar.frame
        // - 40 is editable , the default value is 49 px, below lowers the tabbar and above increases the tab bar size
        if hasTopNotch {
            tabFrame.size.height = 85
            tabFrame.origin.y = self.view.frame.size.height - 85
            self.tabBar.frame = tabFrame}
    }
    var hasTopNotch: Bool {
        if #available(iOS 11.0,  *) {
            return UIApplication.shared.delegate?.window??.safeAreaInsets.top ?? 0 > 20
        }
        return false
    }
}
