//
//  APIS.swift
//  skillzy
//
//  Created by Mohamed Nawar on 8/26/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//
// http://mohamina.elmotahda.site
// http://mohamina.com

import Foundation

class APIs {
    static let Instance = APIs()
     private init() {}

    private let url = "http://mohamina.com"
    
    public func getHeader() -> [String: String]{
        let header = [
            "Accept" : "application/json" , "Authorization" : "Bearer \(User.shared.token )",
            "Accept-Language":"ar"
        ]
        return header
    }
    public func registeration() -> String{
        return url + "/api/auth/register"
    }
    
    public func uploadImages() -> String{
           return url + "/api/tmp_files"
       }
    
    public func login() -> String{
        return url + "/api/auth/login"
    }
    
    public func forgetPassword() -> String{
        return url + "/api/auth/password/forget"
    }
    
    public func checkCode() -> String{
        return url + "/api/auth/password/check-code"
    }
    
    public func resetPassword() -> String{
        return url + "/api/auth/password/reset"
    }
    public func getOrders() -> String{
        return url + "/api/orders"
    }
    public func addAppointment(id:Int) -> String {
         return url + "/api/appointments/\(id)"
    }

    public func updateClientProfile() -> String{
        return url + "/api/profile/client/update"
    }
    
    public func updateLawyerProfile() -> String{
        return url + "/api/profile/lawyer/update"
    }
    
    public func getContactUs() -> String{
        return url + "/api/cards/contacts"
    }
    
    
    public func getCaseTypes() -> String{
        return url + "/api/issue_types"
    }
    
    public func getFilterCards() -> String{
        return url + "/api/cards/filters"
    }
    
    public func getCities() -> String {
        return url + "/api/cities/"
    }
    
    public func getLaywers(id : Int , page : Int) -> String{
        return url + "/api/cities/\(id)/lawyers?page=\(page)"
    }
    
    public func settings() -> String{
        return url + "/api/settings"
    }
    
    public func contact_us() -> String{
        return url + "/api/contact_us"
    }
    
    public func notification() -> String{
        return url + "/api/notifications"
    }
    
    public func updateAppointment(id: Int ) -> String{
        return url + "/api/appointments/\(id)"
    }
    public func appointments_details(_ id: Int ) -> String{
        return url + "/api/appointments/\(id)"
    }
    
    public func getRegulations() -> String{
        return url + "/api/legal_articles"
    }
    
    public func searchRegulations(_ name: String) -> String{
        return url + "/api/legal_articles?name=\(name)"
    }
    
    public func getRegulationData(_ id: Int) -> String{
        return url + "/api/legal_articles/\(id)"
    }
    
    public func searchRegulationData(_ id: Int, and name: String) -> String{
        return url + "/api/legal_articles/\(id)?name=\(name)"
    }
}
