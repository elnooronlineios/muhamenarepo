//
//  LawyersViewController.swift
//  muhamina
//
//  Created by iMac on 10/30/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation
import MapKit
import FCAlertView
class LawyersViewController: UIViewController {
    @IBOutlet var trophies: UILabel!
    @IBOutlet var cases: UILabel!
    @IBOutlet var backgroundImgView: UIImageView!
    @IBOutlet var callBtn: UIButton!
    
    @IBAction func callPressed(_ sender: Any) {
        guard let number = URL(string: "tel://" + (laywer.mobile ?? "")) else { return }
        UIApplication.shared.open(number)

    }
    @IBOutlet var checkMap: UIButton!
    var heroId = ""
    var laywer = Lawyer(){
        didSet{
            self.view.viewWithTag(2)?.hero.id = heroId+"-1"
            view.viewWithTag(1)?.hero.id = heroId+"-2"
            view.viewWithTag(33)?.hero.id = heroId+"-3"
           checkMap.hero.id = heroId+"-4"
            backgroundImgView.hero.id = heroId+"-5"
            print( view.viewWithTag(2)?.hero.id )
            let namelbl = view.viewWithTag(2) as! UILabel
            let cityLbl = view.viewWithTag(1) as! UILabel
            let imgView = view.viewWithTag(33) as! UIImageView
            let certificationsLbl = view.viewWithTag(4) as! UILabel
            let issuesLbl = view.viewWithTag(5) as! UILabel
            namelbl.font = UIFont(name: "NeoSansArabic", size: 14) ?? UIFont.systemFont(ofSize: 15)
            cityLbl.font = UIFont(name: "NeoSansArabic", size: 14) ?? UIFont.systemFont(ofSize: 15)
            certificationsLbl.font = UIFont(name: "Cairo-Regular", size: 14) ?? UIFont.systemFont(ofSize: 15)
            issuesLbl.font = UIFont(name: "Cairo-Regular", size: 14) ?? UIFont.systemFont(ofSize: 15)
            if  let city = laywer.city?.name {
                cityLbl.text = city
            }
            if  let name = laywer.name {
                namelbl.text = name
            }
            if let profileImage = laywer.avatar {
                imgView.sd_setImage(with: URL(string: profileImage), completed: nil)
            }
            if  let laywerCertifications = laywer.certifications {
                var stringValue = ""
                for certification in laywerCertifications {
                    let certificationName = certification.name ?? ""
                    stringValue.append("- \(certificationName)\n")
                }
                print(stringValue)
                certificationsLbl.text = "\(stringValue)"
            }
            if  let laywerCases = laywer.issues {
                var stringValue = ""
                for issue in laywerCases {
                    let issueName = issue.name ?? ""
                    stringValue.append("- \(issueName)\n" )
                }
                print(stringValue)
                issuesLbl.text = "\(stringValue)"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.hero.isEnabled = true
        self.view.viewWithTag(100)?.hero.id = "img02"
        self.hero.isEnabled = true
        trophies.font = UIFont(name: "NeoSansArabic", size: 17) ?? UIFont.systemFont(ofSize: 15)
        cases.font = UIFont(name: "NeoSansArabic", size: 17) ?? UIFont.systemFont(ofSize: 15)
        self.navigationController?.hero.isEnabled = true
//        if User.shared.isLoggedIn() {
//            callBtn.isHidden = false
//        }
        callBtn.isHidden = false
 
    }
    override func viewWillAppear(_ animated: Bool) {
        setupNavBar(vc: self)
    }
    @IBAction func showLawyerLocation(_ sender: Any) {
        //        let navVC = storyboard?.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
        //        if let profileImage = laywer.avatar {
        //            navVC.imgView.sd_setImage(with: URL(string: profileImage), completed: nil)
        //        }
        //        if  let name = laywer.name {
        //            navVC.lawyerName = name
        //        }
        //        let latitude = Double(laywer.latitude ?? "")
        //         let longitude = Double(laywer.longitude ?? "")
        //        navVC.lawyerCoordinate = CLLocationCoordinate2D(latitude: CLLocationDegrees(latitude ?? 0), longitude: CLLocationDegrees(longitude ?? 0))
        //        self.navigationController?.pushViewController(navVC, animated: true)
        
        
        let latitude: CLLocationDegrees = CLLocationDegrees(Double(laywer.latitude ?? "") ?? 0)
        let longitude: CLLocationDegrees = CLLocationDegrees(Double(laywer.longitude ?? "") ?? 0)
        
        let regionDistance:CLLocationDistance = 10000
        let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinates, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = laywer.name ?? ""
        mapItem.openInMaps(launchOptions: options)
        
        
    }
    
    @IBAction func reserveDate(_ sender: Any) {
        if User.shared.isLoggedIn() {
            let vc = storyboard?.instantiateViewController(withIdentifier: "HagzMaw3edViewController") as! HagzMaw3edViewController
            vc.lawyerId = laywer.id ?? 0
            show(vc, sender: self)
            return
        }
        showLoginAlert()
    }
    func showLoginAlert(){
        let alert = FCAlertView()
        alert.makeAlertTypeCaution()
        
        alert.doneBlock = {
            let login =  AppStoryboard.Main.viewController(viewControllerClass: LoginVC.self)
            let navLogin = UINavigationController(rootViewController: login)
            self.present(navLogin, animated: true, completion: nil)
        }
        alert.firstButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.addButton("إنشاء حساب") {
            let vc =  AppStoryboard.Main.viewController(viewControllerClass: LoginVC.self)
            vc.hideButton = false
            vc.showSignup = true
            let navVc = UINavigationController(rootViewController: vc)
            self.present(navVc, animated: true, completion: nil)
        }
        alert.colorScheme = #colorLiteral(red: 0.4895811677, green: 0.1901164651, blue: 0.8794597983, alpha: 1)
        alert.dismissOnOutsideTouch = true
        alert.titleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.subtitleFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.doneButtonCustomFont = UIFont(name: "NeoSansArabic", size: 17)
        alert.showAlert(inView: self, withTitle: "عفواً", withSubtitle: "لا يمكنك اخذ موعد إلا بعد تسجيل الدخول", withCustomImage: #imageLiteral(resourceName: "icon13"), withDoneButtonTitle: NSLocalizedString("تسجيل الدخول", comment: ""), andButtons: nil)
    }
}
