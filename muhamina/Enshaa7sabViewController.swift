//
//  Enshaa7sabViewController.swift
//  muhamina
//
//  Created by MACBOOK on 7/28/18.
//  Copyright © 2018 MuhammedAli. All rights reserved.
//

import UIKit
import TextFieldEffects
import PKHUD
class Enshaa7sabViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet var usernameField: HoshiTextField!
    @IBOutlet var emailField: HoshiTextField!
    @IBOutlet var phoneNumberField: HoshiTextField!
    @IBOutlet var passwordField: HoshiTextField!
    @IBOutlet var verifyPasswordField: HoshiTextField!
    @IBOutlet var addPhotoBtn: UIButton!
    var selectedImageFlag = false
    var pickerController = UIImagePickerController()
    
    @IBAction func dismissView(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    var selectedImage = UIImage()
    @IBAction func addPhotoPressed(_ sender: UIButton) {
        let alertViewController = UIAlertController(title: "", message: NSLocalizedString("اختر خيارا", comment: ""), preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: NSLocalizedString("الكاميرا", comment: ""), style: .default, handler: { (alert) in
            self.openCamera()
        })
        let gallery = UIAlertAction(title: NSLocalizedString("المعرض", comment: ""), style: .default) { (alert) in
            self.openGallary()
        }
        let cancel = UIAlertAction(title: NSLocalizedString("إلغاء", comment: ""), style: .cancel) { (alert) in
            
        }
        alertViewController.addAction(camera)
        alertViewController.addAction(gallery)
        alertViewController.addAction(cancel)
        alertViewController.popoverPresentationController?.sourceView = self.view
        alertViewController.popoverPresentationController?.permittedArrowDirections = .up
        alertViewController.popoverPresentationController?.sourceRect = sender.frame
        self.present(alertViewController, animated: true, completion: nil)
    }
    @IBOutlet var img: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        img.hero.id = "Ali"
        setupNavBar(vc: self)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signUp_btn(_ sender: UIButton) {
        signup()
    }
    func signup(){
        if usernameField.text!.isEmpty == false && emailField.text!.isEmpty == false && phoneNumberField.text!.isEmpty == false && passwordField.text!.isEmpty == false && verifyPasswordField.text!.isEmpty == false && selectedImageFlag {
            if !(emailField.text?.isValidEmail())!{
                makeDoneAlert(title: "البريد الإلكتروني غير صحيح", SubTitle: "من فضلك تأكد من إدخال البريد الإلكتروني بطريقة صحيحة", Image: #imageLiteral(resourceName: "icon13"))
                return
            }
            
            if passwordField.text! != verifyPasswordField.text!{
                makeDoneAlert(title: "", SubTitle: " كلمتا المرور غير متطابقة", Image: #imageLiteral(resourceName: "icon13"))
                return
            }
            
            let header = [
                "Accept" : "application/json"
            ]
            let par = ["name":usernameField.text ?? "",
                        "email": emailField.text ?? "",
                       "password": passwordField.text ?? "",
                       "mobile":phoneNumberField.text ?? "",
                       "avatar_base64":(UIImageJPEGRepresentation(selectedImage, 0.2)?.base64EncodedString())!,
                       "onesignal-player-id":UserDefaults.standard.string(forKey: "onesignalid") ?? ""
                ] as [String : Any]
            print(par)
            
            HUD.show(.progress)
            callApi(withURL: APIs.Instance.registeration(), method: .post, headers: APIs.Instance.getHeader(), Parameter: par) { (result) in
                HUD.hide()
                switch result {
                case .success(let data):
                    UIApplication.shared.registerForRemoteNotifications()
                    UserDefaults.standard.set(data, forKey:"user")
                     UserDefaults.standard.set(false, forKey: "presentedCountryPicker")
                    User.shared.fetchUser()
                    self.dismissWithTransition()
                default:
                    break
                }
            }
        }else{
            makeDoneAlert(title: "حقل فارغ", SubTitle: "من فضلك تأكد من إدخال جميع البيانات", Image: #imageLiteral(resourceName: "icon13"))
            if !selectedImageFlag {
                addPhotoBtn.layer.borderColor = UIColor.red.cgColor
            }
            for view in self.view.subviews as [UIView] {
                if let textField = view as? HoshiTextField {
                    if textField.text!.isEmpty == true{
                        textField.borderActiveColor = UIColor.red
                        textField.borderInactiveColor = UIColor.red
                        
                    }
                }
            }
        }
    }
    
    
    func dismissWithTransition()  {
      
            self.dismiss(animated: false, completion: {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "didLogin"), object: nil, userInfo: nil)
            })
            
        
    }
    
    
    
    func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
            pickerController.delegate = self
            self.pickerController.sourceType = UIImagePickerControllerSourceType.camera
            pickerController.allowsEditing = true
            self .present(self.pickerController, animated: true, completion: nil)
        }
        else {
            
            let alertViewController = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Ok", style: .default) { (alert) in
                
            }
            alertViewController.addAction(cancel)
            self.present(alertViewController, animated: true, completion: nil)
        }
    }
    func openGallary() {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            
            pickerController.delegate = self
            pickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            pickerController.allowsEditing = true
            pickerController.navigationBar.isTranslucent = false
            pickerController.navigationBar.barTintColor = #colorLiteral(red: 0.4459182024, green: 0.1324112117, blue: 0.8598670363, alpha: 1) // Background color
            pickerController.navigationBar.tintColor = .white
            self.present(pickerController, animated: true, completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !isModal{
            self.navigationItem.leftBarButtonItem = nil
        }
    }
    public func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        selectedImageFlag = true
        let image = info[UIImagePickerControllerEditedImage] as! UIImage
        addPhotoBtn.setImage(image, for: .normal)
        selectedImage = image
        dismiss(animated:true, completion: nil)
    }
    public func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
        print("Cancel")
    }

    
}
